﻿using Omentia.Services.Messages.DataAccess;
using Shouldly;
using Xunit;

namespace Omentia.Services.Messages.Tests.UnitTests
{
    public class FakeContactSourceTests
    {
        [Fact]
        public void FakeContactSource_GetAll_ReturnsNonEmptyContactList()
        {
            //Arrange
            var contactSource = new FakeContactSource();

            //Act
            var actualDoctors = contactSource.DoctorsAsync().Result;
            var actualPatients = contactSource.PatientsAsync().Result;

            //Assert
            actualDoctors.ShouldNotBeEmpty();
            actualPatients.ShouldNotBeEmpty();
        }
    }
}