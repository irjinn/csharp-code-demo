﻿using System;
using FizzWare.NBuilder.Generators;
using Helios.Common.Models.Messages;
using Omentia.Services.Messages.Services;
using Shouldly;
using Xunit;

namespace Omentia.Services.Messages.Tests.UnitTests
{
    public sealed class MessageValidatorTests
    {
        private readonly MessageValidator _validator;

        public MessageValidatorTests()
        {
            _validator = new MessageValidator();
        }

        [Fact]
        public void MessageValidator_Validate_Returns_Valid_For_Valid_Message()
        {
            // Arrange
            var msg = new Message
            {
                Body = GetRandom.String(50),
                FromId = Guid.NewGuid(),
                ToId = Guid.NewGuid()
            };

            // Act
            var validationResult = _validator.Validate(msg);

            // Assert
            validationResult.ShouldNotBeNull();
            validationResult.Errors.ShouldBeEmpty();
            validationResult.IsValid.ShouldBeTrue();
        }

        [Theory]
        [InlineData(null)]
        [InlineData("")]
        [InlineData(" ")]
        public void MessageValidator_Validate_Invalid_When_Body_Is_Empty(string body)
        {
            // Arrange
            var msg = new Message { Body = body };

            // Act
            var validationResult = _validator.Validate(msg);

            // Assert
            validationResult.ShouldNotBeNull();
            validationResult.IsValid.ShouldBeFalse();
            validationResult.Errors.ShouldContain(f => 
                f.ErrorMessage == MessageServiceRes.MessageBodyCannotBeEmpty && 
                f.PropertyName == nameof(Message.Body) );
        }
        
        [Fact]
        public void MessageValidator_Validate_Invalid_When_FromId_Is_Empty()
        {
            // Arrange
            var msg = new Message { Body = "text", FromId = Guid.Empty };

            // Act
            var validationResult = _validator.Validate(msg);

            // Assert
            validationResult.ShouldNotBeNull();
            validationResult.IsValid.ShouldBeFalse();
            var text = MessageServiceRes.MessageFromIdCannotBeEmpty;
            validationResult.Errors.ShouldContain(f =>
                f.ErrorMessage == text &&
                f.PropertyName == nameof(Message.FromId));
        }
    }
}