﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation.Results;
using Helios.Common;
using Helios.Common.Models.Messages;
using Helios.Common.Notifications;
using Moq;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Services;
using Shouldly;
using Xunit;

namespace Omentia.Services.Messages.Tests.UnitTests
{
    [Trait("Category", "Messages")]
    public sealed class MessageServiceTests
    {
        private Message Message { get; } = new Message
        {
            Body = "text",
            FromId = Guid.NewGuid()
        };

        private class MessageServiceParams
        {
            public Mock<IMessageStorageFacade> Storage { get; }
            public Mock<IMessageValidator> MessageValidator { get; }
            public Mock<INotificationService> NotificationService { get; }

            public MessageServiceParams(Message message,
                Action<Mock<IMessageValidator>> validatorSetup = null,
                Action<Mock<IMessageStorageFacade>> storageSetup = null
                )
            {
                Storage = new Mock<IMessageStorageFacade>();
                if (storageSetup != null)
                {
                    storageSetup(Storage);
                }
                else
                {
                    Storage
                        .Setup(r => r.AddMessageAsync(It.IsAny<Message>()))
                        .Returns<Message>(Task.FromResult);
                }
                MessageValidator = new Mock<IMessageValidator>();
                if (validatorSetup != null)
                {
                    validatorSetup(MessageValidator);
                }
                else
                {
                    MessageValidator
                        .Setup(v => v.ValidateAsync(It.IsAny<Message>(), default(CancellationToken)))
                        .ReturnsAsync(new ValidationResult {Errors = {}});
                }
                NotificationService = new Mock<INotificationService>();
            }
        }

        private MessageService CreateService(MessageServiceParams @params)
        {
            return new MessageService(
                @params.MessageValidator.Object,
                @params.Storage.Object,
                @params.NotificationService.Object);
        }
        
        [Fact]
        public void MessageService_SendPatientMessage_ThrowsException_When_Validation_Fails()
        {
            // Arrange
            var message = Message;
            const string propertyName = "property";
            const string errorText = "property is invalid";
            Action<Mock<IMessageValidator>> setup =
                mock => mock
                    .Setup(v => v.ValidateAsync(It.IsAny<Message>(), default(CancellationToken)))
                    .ReturnsAsync(new ValidationResult(new[] {new ValidationFailure(propertyName, errorText)}));
            var @params = new MessageServiceParams(message, validatorSetup: setup);
            var service = CreateService(@params);

            // Act
            Func<Task<Message>> result = () => service.SendPatientMessageAsync(message);

            // Assert
            var exception = Assert.ThrowsAsync<ValidationException<Message>>(result);
            exception.Result.ValidationResult.ShouldNotBe(null);
            exception.Result.ValidationResult.IsValid.ShouldBeFalse();
            exception.Result.ValidationResult.Errors.Count.ShouldBe(1);
            exception.Result.Message.ShouldBe(ExceptionTextBuilder.ValidationFailedForType<Message>());
            exception.Result.ValidationResult.Errors.ShouldContain(e => e.ErrorMessage == errorText);
            exception.Result.ValidationResult.Errors.ShouldContain(e => e.PropertyName == propertyName);
        }
    }
}