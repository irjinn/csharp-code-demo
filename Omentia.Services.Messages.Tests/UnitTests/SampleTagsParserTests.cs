﻿using System.Linq;
using Omentia.Services.Messages.TagsParsers;
using Shouldly;
using Xunit;
using Tags = Omentia.Services.Messages.TagsParsers.SampleTagsParser.Tags;

namespace Omentia.Services.Messages.Tests.UnitTests
{
    public sealed class SampleTagsParserTests
    {
        public SampleTagsParserTests()
        {
            _parser = new SampleTagsParser();
        }

        private readonly SampleTagsParser _parser;

        public static object[] SampleParserTestData =
        {
            new object[] { "Jag behöver förnya mitt recept", new [] { Tags.RecipeRenewal }, 1 },
            new object[] { "Jag behöver förnya min medicin", new[] { Tags.RecipeRenewal }, 1 },
            new object[] { "Min medicin är slut", new[] { Tags.RecipeRenewal }, 1 },
            new object[] { "Hur går det med labbet?", new[] { Tags.LabResults }, 1 },
            new object[] { "Hur går det med provsvaret och mitt nya recept?", new[] { Tags.LabResults, Tags.RecipeRenewal }, 2 },
            new object[] { "lorem ipsum", new[] { Tags.Other }, 1 },
            new object[] { "", new string[0], 0 },
            new object[] { "   ", new string[0], 0 },
            new object[] { null, new string[0], 0 }
        };

        [Theory]
        [MemberData(nameof(SampleParserTestData))]
        public void SampleTagsParser_Parsers_Tags(string message, string[] expectedTags, int tagsCount)
        {
            // Arrange

            // Act
            var tags = _parser.ParseMessage(message).ToList();

            // Assert
            if (tags.Count > 0)
            {
                foreach (var tag in expectedTags)
                {
                    tags.ShouldContain(tag);
                }
            }
            tags.Count.ShouldBe(tagsCount);
        }
    }
}
