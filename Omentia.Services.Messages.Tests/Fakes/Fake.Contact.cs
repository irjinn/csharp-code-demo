﻿using FizzWare.NBuilder;
using Helios.Common.Models;
using Helios.Common.Models.Contacts;
using Omentia.Services.Messages.DataAccess;

namespace Omentia.Services.Messages.Tests.Fakes
{
    public static partial class Fake //Contact
    {
        private static readonly FakeContactSource ContactsSource = new FakeContactSource();
        public static Contact DoctorContact()
        {
            return Pick<Contact>.RandomItemFrom(ContactsSource.DoctorsAsync().Result).SetId(Fake.Random.Id());
        }
        public static Contact PatientContact()
        {
            return Pick<Contact>.RandomItemFrom(ContactsSource.PatientsAsync().Result).SetId(Fake.Random.Id());
        }
    }
}