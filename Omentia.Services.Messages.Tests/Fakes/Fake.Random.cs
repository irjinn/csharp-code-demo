﻿using System;
using FizzWare.NBuilder.Generators;
using Helios.Common.Utils;

namespace Omentia.Services.Messages.Tests.Fakes
{
    public static partial class Fake // Random
    {
        public static class Random
        {
            public static Guid Id() => Guid.NewGuid();
            public static string Sentence()
                => GetRandom.Phrase(GetRandom.Int(20, 30));
            public static string Name()
                => GetRandom.String(GetRandom.Int(5, 10)).UppercaseFirst();
        }
    }
}