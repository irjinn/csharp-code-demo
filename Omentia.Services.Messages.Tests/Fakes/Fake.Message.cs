﻿using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Tests.Fakes
{
    public static partial class Fake // Message
    {
        public static Message Message()
        {
            return new Message()
            {
                ToId = Random.Id(),
                FromId = Random.Id(),
                Body = Random.Sentence(),
            };
        }
    }
}