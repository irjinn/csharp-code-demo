﻿using System;
using System.Threading.Tasks;
using Omentia.Services.Messages.Extensions;
//using Omentia.Services.Messages.Extensions;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Tests.Fakes;
using Shouldly;
using Xunit;

namespace Omentia.Services.Messages.Tests
{
    public class MessageServiceFixture : IDisposable
    {
        public IntegrationTestContext Context { get; } = new IntegrationTestContext("messages");
        public void Dispose()
        {
            Context.CleanUp();
        }
    }
    public class MessageServiceTests : IClassFixture<MessageServiceFixture>
    {
        public IntegrationTestContext Context { get; }
        public IConversationSource Conversations => Context.Resolve<IConversationSource>();
        public IMessageService Service => Context.Resolve<IMessageService>();

        public MessageServiceTests(MessageServiceFixture fixture)
        {
            Context = fixture.Context;
        }

        [Fact] //check that correct conversation is implicitly created
        public async Task MessageService_SendMessageByDoctor_CreatesConversationWithMessage()
        {
            //Arrange
            var patient = Context.GetPatient();
            var doctor = Context.RunAsDoctor();

            var doctorMessage = Fake.Message().SetFrom(doctor.Id).SetTo(patient.Id);

            //Act
            var addedMessage = await Service.SendDoctorMessageAsync(doctorMessage);
            var conversation = await Conversations.FindOne(doctor.Id, patient.Id);
            var conversationDetails = await Conversations.Details(conversation.Id);

            //Assert
            addedMessage.ConversationId.ShouldBe(conversation.Id);
            conversation.LastMessage.ShouldBe(doctorMessage);
            conversationDetails.Messages.ShouldContain(doctorMessage);
        }

        [Fact]
        public async Task MessageService_SendMessageByDoctor_MessageAddedToExistingConversation()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            var doctorMessage = Fake.Message().SetFrom(doctor.Id).SetTo(patient.Id);
            await Service.SendPatientMessageAsync(patientMessage);
            Context.RunAs(doctor);

            //Act
            var addedMessage = await Service.SendDoctorMessageAsync(doctorMessage);
            var conversation = await Conversations.FindOne(doctor.Id, patient.Id);
            var conversationDetails = await Conversations.Details(conversation.Id);

            //Assert
            addedMessage.ConversationId.ShouldBe(conversation.Id);
            conversation.LastMessage.ShouldBe(doctorMessage);
            conversationDetails.Messages.ShouldContain(doctorMessage);
            conversationDetails.Messages.ShouldContain(patientMessage);
        }

        [Fact]
        public async Task MessageService_SendMessageByDoctor_DoctorConversationReadAndAnswered()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            var doctorMessage = Fake.Message().SetFrom(doctor.Id).SetTo(patient.Id);

            await Service.SendPatientMessageAsync(patientMessage);
            var expected = await Conversations.FindOne(doctor.Id, patient.Id);
            Context.RunAs(doctor);

            //Act
            await Service.SendDoctorMessageAsync(doctorMessage);

            var read = await Conversations.WithDoctor(doctor.Id, read: true);
            var answered = await Conversations.WithDoctor(doctor.Id, answered: true);
            var readAnswered = await Conversations.WithDoctor(doctor.Id, read: true, answered: true);

            var unread = await Conversations.WithDoctor(doctor.Id, read: false);
            var unanswered = await Conversations.WithDoctor(doctor.Id, answered: false);
            var unreadUnanswered = await Conversations.WithDoctor(doctor.Id, read: false, answered: false);

            //Assert
            read.ShouldContain(expected);
            answered.ShouldContain(expected);
            readAnswered.ShouldContain(expected);

            unread.ShouldNotContain(expected);
            unanswered.ShouldNotContain(expected);
            unreadUnanswered.ShouldNotContain(expected);
        }

        [Fact]
        public async Task MessageService_SendMessageByDoctor_PatientConversationUnread()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            var doctorMessage = Fake.Message().SetFrom(doctor.Id).SetTo(patient.Id);

            await Service.SendPatientMessageAsync(patientMessage);
            var expected = await Conversations.FindOne(doctor.Id, patient.Id);
            var originalUnreadCount = await Conversations.TotalUnread(patient.Id);
            Context.RunAs(doctor);

            //Act
            await Service.SendDoctorMessageAsync(doctorMessage);
            Context.RunAs(patient);

            var read = await Conversations.WithPatient(patient.Id, read: true);
            var unread = await Conversations.WithPatient(patient.Id, read: false);
            var unreadCount = await Conversations.TotalUnread(patient.Id);

            //Assert
            read.ShouldNotContain(expected);
            unread.ShouldContain(expected);
            unreadCount.ShouldBe(originalUnreadCount + 1);
        }
        
        [Fact]
        public async Task MessageService_ReadConversationByDoctor_DoctorConversationMarkedRead()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            var lastMessage = await Service.SendPatientMessageAsync(patientMessage);
            var expected = await Conversations.FindOne(doctor.Id, patient.Id);

            Context.RunAs(doctor);

            //Act
            await Service.SetMessageIsRead(lastMessage.Id);

            var read = await Conversations.WithDoctor(doctor.Id, read: true);
            var unread = await Conversations.WithDoctor(doctor.Id, read: false);

            //Assert
            read.ShouldContain(expected);
            unread.ShouldNotContain(expected);
        }

        [Fact] //check that correct conversation is implicitly created
        public async Task MessageService_SendMessageByPatient_CreatesConversationWithMessage()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);

            //Act
            var addedMessage = await Service.SendPatientMessageAsync(patientMessage);
            var conversation = await Conversations.FindOne(doctor.Id, patient.Id);
            var conversationDetails = await Conversations.Details(conversation.Id);

            //Assert
            addedMessage.ConversationId.ShouldBe(conversation.Id);
            conversation.LastMessage.ShouldBe(patientMessage);
            conversationDetails.Messages.ShouldContain(patientMessage);
        }

        [Fact]
        public async Task MessageService_SendMessageByPatient_DoctorConversationUnreadAndUnanswered()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            await Service.SendPatientMessageAsync(patientMessage);
            var expected = await Conversations.FindOne(doctor.Id, patient.Id);
            Context.RunAs(doctor);

            //Act
            var unread = await Conversations.WithDoctor(doctor.Id, read: false);
            var unanswered = await Conversations.WithDoctor(doctor.Id, answered: false);
            var unreadUnanswered = await Conversations.WithDoctor(doctor.Id, read: false, answered: false);

            //Assert
            unreadUnanswered.ShouldContain(expected);
            unread.ShouldContain(expected);
            unanswered.ShouldContain(expected);
        }
        
        [Fact]
        public async Task MessageService_ReadConversationByPatient_PatientConversationMarkedRead()
        {
            //Arrange
            var patient = Context.RunAsPatient();
            var doctor = Context.GetDoctor();

            var patientMessage = Fake.Message().SetFrom(patient.Id).SetTo(doctor.Id);
            var doctorMessage = Fake.Message().SetFrom(doctor.Id).SetTo(patient.Id);

            await Service.SendPatientMessageAsync(patientMessage);
            var expected = await Conversations.FindOne(doctor.Id, patient.Id);

            Context.RunAs(doctor);
            var lastMessage = await Service.SendDoctorMessageAsync(doctorMessage);

            Context.RunAs(patient);
            var originalUnreadCount = await Conversations.TotalUnread(patient.Id);

            //Act
            await Service.SetMessageIsRead(lastMessage.Id);

            var read = await Conversations.WithPatient(patient.Id, read: true);
            var unread = await Conversations.WithPatient(patient.Id, read: false);
            var unreadCount = await Conversations.TotalUnread(patient.Id);

            //Assert
            read.ShouldContain(expected);
            unread.ShouldNotContain(expected);
            unreadCount.ShouldBe(originalUnreadCount - 1);
        }
    }
}