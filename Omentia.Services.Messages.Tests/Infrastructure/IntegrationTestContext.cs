﻿using System;
using Autofac;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Http.Client;
using Helios.Common.Http.Client.TestUtils;
using Helios.Common.Models.Contacts;
using Omentia.Services.Messages.Api;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Tests.Fakes;

namespace Omentia.Services.Messages.Tests
{
    public class IntegrationTestContext
    {
        private readonly string _contextName;
        private static string RandomContextName => "integration_" + Guid.NewGuid().ToString().Substring(0, 8);
        private IContainer Container { get; }
        private IMessageStorageFacade Storage => Container.Resolve<IMessageStorageFacade>();
        private InMemoryContactsClient Contacts { get; } = new InMemoryContactsClient();

        public TResult Resolve<TResult>() => Container.Resolve<TResult>();

        public IntegrationTestContext() : this(RandomContextName)
        {
        }

        public IntegrationTestContext(string contextName)
        {
            _contextName = contextName;
            Container = BuildContainer();
        }

        public Contact RunAs(Contact contact)
        {
            //set up security context here
            return contact;
        }

        public Contact RunAsDoctor() => RunAs(GetDoctor());
        public Contact RunAsPatient() => RunAs(GetPatient());

        public Contact GetDoctor() => CreateContact(ContactType.Doctor);
        public Contact GetPatient() => CreateContact(ContactType.Patient);

        public void CleanUp()
        {
            IDocumentDatabase db = null;
            IDocumentStorage storage = null;
            try
            {
                db = Resolve<IDocumentDatabase>();
                storage = Resolve<IDocumentStorage>();
            }
            catch (Exception)
            {
                //ignore
            }
            storage?.DropDatabase(db?.DatabaseName);
        }

        private Contact CreateContact(ContactType contactType)
        {
            var contact = contactType == ContactType.Patient
                ? Fake.PatientContact()
                : Fake.DoctorContact();

            Contacts.Add(contact);

            return contact;
        }

        private IContainer BuildContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new MessagesApiModule());
            builder
                .Register(c => new DocumentDatabaseConfiguration { DatabaseName = $"test-{_contextName}" })
                .As<IDocumentDatabaseConfiguration>();
            builder.RegisterInstance(Contacts).As<IContactsClient>();

            return builder.Build();
        }
    }
}