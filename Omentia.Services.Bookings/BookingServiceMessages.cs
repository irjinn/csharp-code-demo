﻿using System;

namespace Omentia.Services.Bookings
{
    public static class BookingServiceMessages
    {
        public static string OpenedVideoSessionForBookingNotFound(Guid bookingId)
            => string.Format(BookingServiceRes.VideoSessionForBookingNotFound, bookingId);
    }
}