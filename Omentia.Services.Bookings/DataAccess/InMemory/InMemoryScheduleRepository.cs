﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.DataAccess
{
    public class InMemoryScheduleRepository : IScheduleRepository
    {
        public IDictionary<Guid, WeekSchedule> SchedulesByDoctor { get; }
            = new ConcurrentDictionary<Guid, WeekSchedule>();

        public Task<WeekSchedule> FindScheduleByDoctorAsync(Guid doctorId)
            => Task.FromResult(SchedulesByDoctor.SafeGet(doctorId));

        public Task<WeekSchedule> UpsertScheduleAsync(WeekSchedule schedule)
            => Task.FromResult(SchedulesByDoctor[schedule.DoctorId] = schedule);
    }
}