﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.DataAccess.Sql
{
    public static class BookingsDbDefinition
    {
        public static DbModelBuilder Configure(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder
                .ConfigureTimeslots()
                .ConfigureBookings()
                .ConfigureVideoSessions()
                .ConfigurePayments();

            return modelBuilder;
        }

        public static DbModelBuilder ConfigureTimeslots(this DbModelBuilder builder)
        {
            var entity = builder.Entity<Timeslot>();
            entity.Property(x => x.ChangeDate).IsConcurrencyToken();
            entity
                .HasOptional(x => x.ActualBooking)
                .WithOptionalDependent()
                .Map(x => x.MapKey("ActualBookingId"))
                .WillCascadeOnDelete(false);
            
            return builder;
        }
        public static DbModelBuilder ConfigureBookings(this DbModelBuilder builder)
        {
            var entity = builder.Entity<Booking>();
            entity.Property(x => x.ChangeDate).IsConcurrencyToken();

            entity
                .HasRequired(x => x.Timeslot)
                .WithMany(x => x.Bookings)
                .Map(x => x.MapKey("TimeslotId"))
                .WillCascadeOnDelete(false);

            entity
                .HasOptional(x => x.VideoSession)
                .WithOptionalDependent(x => x.Booking)
                .Map(x => x.MapKey("VideoSessionId"))
                .WillCascadeOnDelete(false);

            entity
                .HasOptional(x => x.Payment)
                .WithOptionalDependent(x => x.Booking)
                .Map(x => x.MapKey("PaymentId"))
                .WillCascadeOnDelete(false);

            return builder;
        }
        public static DbModelBuilder ConfigureVideoSessions(this DbModelBuilder builder)
        {
            var entity = builder.Entity<VideoSession>();
            entity.Property(x => x.ChangeDate).IsConcurrencyToken();

            return builder;
        }
        public static DbModelBuilder ConfigurePayments(this DbModelBuilder builder)
        {
            var entity = builder.Entity<Payment>();
            entity
                .HasOptional(x => x.Invoice)
                .WithOptionalDependent()
                .Map(x => x.MapKey("InvoiceId"))
                .WillCascadeOnDelete(false);
            
            return builder;
        }
    }
}