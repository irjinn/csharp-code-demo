namespace Omentia.Services.Bookings.DataAccess.Sql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInvoices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Invoice",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Address = c.String(),
                        InvoiceStatus = c.Int(nullable: false),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ChangeDate = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Payment", "InvoiceId", c => c.Guid());
            CreateIndex("dbo.Payment", "InvoiceId");
            AddForeignKey("dbo.Payment", "InvoiceId", "dbo.Invoice", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payment", "InvoiceId", "dbo.Invoice");
            DropIndex("dbo.Payment", new[] { "InvoiceId" });
            DropColumn("dbo.Payment", "InvoiceId");
            DropTable("dbo.Invoice");
        }
    }
}
