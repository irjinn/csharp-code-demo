using System.Data.Entity.Migrations;

namespace Omentia.Services.Bookings.DataAccess.Sql.Migrations
{
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Booking",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PatientId = c.Guid(nullable: false),
                        DoctorId = c.Guid(nullable: false),
                        PatientComment = c.String(),
                        Activity = c.Int(),
                        PaymentId = c.Guid(),
                        VisitStatus = c.Int(nullable: false),
                        BookingDate = c.DateTimeOffset(precision: 7),
                        ExpirationDate = c.DateTimeOffset(precision: 7),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ChangeDate = c.DateTimeOffset(precision: 7),
                        IsDeleted = c.Boolean(nullable: false),
                        TimeslotId = c.Guid(nullable: false),
                        VideoSessionId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                //.ForeignKey("dbo.Payment", t => t.PaymentId)
                .ForeignKey("dbo.Timeslot", t => t.TimeslotId)
                //.ForeignKey("dbo.VideoSession", t => t.VideoSessionId)
                .Index(t => t.PaymentId)
                .Index(t => t.TimeslotId)
                .Index(t => t.VideoSessionId);
            
            CreateTable(
                "dbo.Payment",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        PaymentStatus = c.Int(nullable: false),
                        PaymentType = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Timeslot",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DoctorId = c.Guid(nullable: false),
                        CareUnitId = c.Guid(nullable: false),
                        Date = c.DateTimeOffset(nullable: false, precision: 7),
                        FromDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ToDate = c.DateTimeOffset(nullable: false, precision: 7),
                        Availability = c.Int(nullable: false),
                        Outcome = c.Int(),
                        ChangeDate = c.DateTimeOffset(precision: 7),
                        ActualBookingId = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                //.ForeignKey("dbo.Booking", t => t.ActualBookingId)
                .Index(t => t.ActualBookingId);
            
            CreateTable(
                "dbo.VideoSession",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        DoctorId = c.Guid(nullable: false),
                        PatientId = c.Guid(),
                        PlannedEndDate = c.DateTimeOffset(precision: 7),
                        ActualEndDate = c.DateTimeOffset(precision: 7),
                        ServiceApiKey = c.String(),
                        ServiceSessionId = c.String(),
                        ServiceAccessToken = c.String(),
                        CreatedDate = c.DateTimeOffset(nullable: false, precision: 7),
                        ChangeDate = c.DateTimeOffset(precision: 7),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);

            Sql(@"ALTER TABLE [dbo].[Booking] ADD CONSTRAINT [FK_dbo.Booking_dbo.Payment_PaymentId] 
            FOREIGN KEY ([PaymentId]) REFERENCES [dbo].[Payment] ([Id]) 
            ON UPDATE NO ACTION ON DELETE SET NULL");

            Sql(@"ALTER TABLE [dbo].[Booking] ADD CONSTRAINT [FK_dbo.Booking_dbo.VideoSession_VideoSessionId] 
            FOREIGN KEY ([VideoSessionId]) REFERENCES [dbo].[VideoSession] ([Id]) 
            ON UPDATE NO ACTION ON DELETE SET NULL");

            Sql(@"ALTER TABLE [dbo].[Timeslot] ADD CONSTRAINT [FK_dbo.Timeslot_dbo.Booking_ActualBookingId] 
            FOREIGN KEY ([ActualBookingId]) REFERENCES [dbo].[Booking] ([Id]) 
            ON UPDATE NO ACTION ON DELETE SET NULL");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Booking", "VideoSessionId", "dbo.VideoSession");
            DropForeignKey("dbo.Booking", "TimeslotId", "dbo.Timeslot");
            DropForeignKey("dbo.Timeslot", "ActualBookingId", "dbo.Booking");
            DropForeignKey("dbo.Booking", "PaymentId", "dbo.Payment");
            DropIndex("dbo.Timeslot", new[] { "ActualBookingId" });
            DropIndex("dbo.Booking", new[] { "VideoSessionId" });
            DropIndex("dbo.Booking", new[] { "TimeslotId" });
            DropIndex("dbo.Booking", new[] { "PaymentId" });
            DropTable("dbo.VideoSession");
            DropTable("dbo.Timeslot");
            DropTable("dbo.Payment");
            DropTable("dbo.Booking");
        }
    }
}
