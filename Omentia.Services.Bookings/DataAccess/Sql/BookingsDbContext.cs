﻿using System.Data.Entity;
using Helios.Common.Infrastructure.Settings;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.DataAccess.Sql
{
    public class BookingsDbContext : DbContext, IBookingsDbContext
    {
        public IDbSet<Timeslot> Timeslots { get; set; }
        public IDbSet<Booking> Bookings { get; set; }
        public IDbSet<VideoSession> VideoSessions { get; set; }
        public IDbSet<Payment> Payments { get; set; }
        public IDbSet<Invoice> Invoices { get; set; }

        public BookingsDbContext() : base(ConnectionStringNames.Sql)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        public BookingsDbContext(string connectionString) : base(connectionString)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
            => BookingsDbDefinition.Configure(modelBuilder);
    }
}