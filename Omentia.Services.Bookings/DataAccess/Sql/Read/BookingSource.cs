﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.DataAccess.Sql.Read
{
    public class BookingSource : IBookingSource
    {
        public IRequestContext Context { get; }
        private IBookingsDbContext Db => Context.Sql;
        
        public BookingSource(IRequestContext context)
        {
            Context = context;
        }

        public IEnumerable<Booking> Find(BookingFilter filter)
        {
            return Db.Bookings.AsNoTracking()
                .Include(x => x.Timeslot)
                .Include(x => x.VideoSession)
                .Include(x => x.Payment.Invoice)
                .Where(x => !x.IsDeleted)
                .Where(x => x.Timeslot != null)
                .ApplyFilter(filter)
                .OrderBy(x => x.BookingDate);
        }

        public int Count(BookingFilter filter)
        {
            return Db.Bookings
                .Where(x => !x.IsDeleted)
                .Where(x => x.Timeslot != null)
                .ApplyFilter(filter)
                .Count();
        }
    }

    public static class BookingFilterExtensions
    {
        public static IQueryable<Booking> ApplyFilter(this IQueryable<Booking> source, BookingFilter filter)
        {
            if (filter == null) return source;
            return source
                .FilterDoctor(filter.DoctorId)
                .FilterPatient(filter.PatientId)
                .FilterIsCancelled(filter.IsCancelled)
                .FilterHasPayment(filter.HasPayment)
                .FilterHasInvoice(filter.HasInvoice)
                .FilterIsInvoiceSent(filter.IsInvoiceSent)
                .FilterIsExpired(filter.IsExpired)
                .FilterInvoiceStatus(filter.InvoiceStatus)
                .FilterIsActive(filter.IsActive);
        }
    }
}