﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.DataAccess.Sql.Read
{
    public class TimeslotSource : ITimeslotSource
    {
        public IRequestContext Context { get; }
        private IBookingsDbContext Db => Context.Sql;

        public TimeslotSource(IRequestContext context)
        {
            Context = context;
        }

        public IEnumerable<Timeslot> Find(TimeslotFilter filter)
        {
            return Db.Timeslots.AsNoTracking()
                .Include(x => x.ActualBooking)
                .ApplyFilter(filter)
                .OrderBy(x => x.FromDate);
        }
    }

    public static class TimeslotFilterExtensions
    {
        public static IQueryable<Timeslot> ApplyFilter(this IQueryable<Timeslot> source, TimeslotFilter filter)
        {
            if (filter == null) return source;
            return source
                .FilterDoctor(filter.DoctorId)
                .FilterBooked(filter.IsBooked)
                .FilterDaysAhead(filter.Days)
                .FilterInFuture(filter.IsInFuture)
                .FilterAvailable(filter.IsAvailableForVideo);
        }
    }
}