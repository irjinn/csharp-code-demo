﻿using System;
using System.Linq;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.DataAccess.Sql.Read
{
    public static partial class QueryableExtensions // Timeslot
    {
        public static IQueryable<Timeslot> FilterDoctor(this IQueryable<Timeslot> source, Guid? doctorId)
        {
            if (doctorId == null) return source;
            return source.Where(x => x.DoctorId == doctorId);
        }
        public static IQueryable<Timeslot> FilterBooked(this IQueryable<Timeslot> source, bool? isBooked)
        {
            if (isBooked == null) return source;
            return isBooked.Value
                ? source.Where(x => x.ActualBooking != null
                    && (x.ActualBooking.ExpirationDate == null || x.ActualBooking.ExpirationDate > DateTimeOffset.Now))
                : source.Where(x => x.ActualBooking == null
                    || (x.ActualBooking.ExpirationDate != null && x.ActualBooking.ExpirationDate <= DateTimeOffset.Now));
        }
        public static IQueryable<Timeslot> FilterDaysAhead(this IQueryable<Timeslot> source, int? days)
        {
            if (days == null) return source;
            var maxDate = DateTimeOffset.Now.AddDays(days.Value + 1);
            return source.Where(x => x.Date < maxDate);
        }
        public static IQueryable<Timeslot> FilterInFuture(this IQueryable<Timeslot> source, bool? isInFuture)
        {
            if (isInFuture == null) return source;
            var minFutureDate = DateTimeOffset.Now.AddMinutes(BookingSettings.Instance.BookingDeadlineMinutes);
            return isInFuture.Value
                ? source.Where(x => x.FromDate > minFutureDate)
                : source.Where(x => x.FromDate < DateTimeOffset.Now);
        }
        public static IQueryable<Timeslot> FilterAvailable(this IQueryable<Timeslot> source, bool? isAvailableForVideo)
        {
            if (isAvailableForVideo == null) return source;
            return isAvailableForVideo.Value
                ? source.Where(x => (x.Availability & DoctorActivity.VideoCall) == DoctorActivity.VideoCall)
                : source.Where(x => (x.Availability & DoctorActivity.VideoCall) != DoctorActivity.VideoCall);
        }
    }
}