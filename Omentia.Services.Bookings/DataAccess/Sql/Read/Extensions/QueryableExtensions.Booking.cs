﻿using System;
using System.Linq;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.DataAccess.Sql.Read
{
    public static partial class QueryableExtensions // Booking
    {
        public static IQueryable<Booking> FilterDoctor(this IQueryable<Booking> source, Guid? doctorId)
        {
            if (doctorId == null) return source;
            return source.Where(x => x.DoctorId == doctorId);
        }
        public static IQueryable<Booking> FilterPatient(this IQueryable<Booking> source, Guid? patientId)
        {
            if (patientId == null) return source;
            return source.Where(x => x.PatientId == patientId);
        }
        public static IQueryable<Booking> FilterIsActive(this IQueryable<Booking> source, bool? isActive)
        {
            if (isActive == null) return source;
            return isActive.Value
                ? source.Active()
                : source.History();
        }
        public static IQueryable<Booking> FilterIsCancelled(this IQueryable<Booking> source, bool? isCancelled)
        {
            if (isCancelled == null) return source;
            return isCancelled.Value
                ? source.Where(x => (x.VisitStatus & VisitStatus.Cancelled) == VisitStatus.Cancelled)
                : source.Where(x => (x.VisitStatus & VisitStatus.Cancelled) != VisitStatus.Cancelled);
        }
        public static IQueryable<Booking> FilterHasPayment(this IQueryable<Booking> source, bool? hasPayment)
        {
            if (hasPayment == null) return source;
            return hasPayment.Value
                ? source.Where(x => x.Payment != null)
                : source.Where(x => x.Payment == null);
        }
        public static IQueryable<Booking> FilterHasInvoice(this IQueryable<Booking> source, bool? hasInvoice)
        {
            if (hasInvoice == null) return source;
            return hasInvoice.Value
                ? source.Where(x => x.Payment.Invoice != null)
                : source.Where(x => x.Payment.Invoice == null);
        }
        public static IQueryable<Booking> FilterIsInvoiceSent(this IQueryable<Booking> source, bool? isInvoiceSent)
        {
            if (isInvoiceSent == null) return source;
            return isInvoiceSent.Value
                ? source.Where(x => x.Payment.Invoice.InvoiceStatus == InvoiceStatus.Sent)
                : source.Where(x => x.Payment.Invoice.InvoiceStatus != InvoiceStatus.Sent);
        }
        public static IQueryable<Booking> FilterIsExpired(this IQueryable<Booking> source, bool? isExpired)
        {
            if (isExpired == null) return source;
            return isExpired.Value
                ? source.Where(x => x.ExpirationDate != null && x.ExpirationDate < DateTimeOffset.Now)
                : source.Where(x => x.ExpirationDate == null || x.ExpirationDate > DateTimeOffset.Now);
        }
        public static IQueryable<Booking> FilterInvoiceStatus(this IQueryable<Booking> source, InvoiceStatus? invoiceStatus)
        {
            if (invoiceStatus == null) return source;
            return source.Where(x => x.Payment.Invoice.InvoiceStatus == invoiceStatus);
        }
        
        /// <remarks>Must be in sync with booking.IsActive() extension method</remarks>
        public static IQueryable<Booking> Active(this IQueryable<Booking> source)
            => source.Where(x =>
                   (x.VisitStatus & VisitStatus.Ended) != VisitStatus.Ended
                && (x.VisitStatus & VisitStatus.Cancelled) != VisitStatus.Cancelled
                && x.Timeslot.ToDate > DateTimeOffset.Now
                && (x.ExpirationDate == null || x.ExpirationDate > DateTimeOffset.Now));
        
        /// <remarks>Must be in sync with booking.IsActive() extension method</remarks>
        /// <remarks>Does not return expired bookings.</remarks>
        public static IQueryable<Booking> History(this IQueryable<Booking> source)
            => source.Where(x =>
                (x.VisitStatus & VisitStatus.Ended) == VisitStatus.Ended
                || (x.VisitStatus & VisitStatus.Cancelled) == VisitStatus.Cancelled
                || x.Timeslot.ToDate <= DateTimeOffset.Now);
    }
}