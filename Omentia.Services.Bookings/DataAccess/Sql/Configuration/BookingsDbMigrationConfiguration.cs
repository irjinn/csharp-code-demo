﻿using System.Data.Entity.Migrations;

namespace Omentia.Services.Bookings.DataAccess.Sql.Configuration
{
    public sealed class BookingsDbMigrationConfiguration : DbMigrationsConfiguration<BookingsDbContext>
    {
        public static bool UseAutomaticMigrations { get; set; }

        public BookingsDbMigrationConfiguration()
        {
            AutomaticMigrationsEnabled = false;
#if DEBUG
            if (UseAutomaticMigrations)
            {
                AutomaticMigrationsEnabled = true;
                AutomaticMigrationDataLossAllowed = true;
            }
#endif
            MigrationsDirectory = "DataAccess\\Sql\\Migrations";
            MigrationsNamespace = "Omentia.Services.Bookings.DataAccess.Sql.Migrations";
        }
    }
}