﻿using System.Data.Entity;
using Omentia.Services.Bookings.DataAccess.Sql.Configuration;

namespace Omentia.Services.Bookings.DataAccess.Sql
{
    public class BookingsDb
    {
        public static void Initialize(bool autoMigrateDatabase = false)
        {
            BookingsDbMigrationConfiguration.UseAutomaticMigrations = false;

            using (var context = new BookingsDbContext())
            {
                if (autoMigrateDatabase)
                {
                    var migrate = new MigrateDatabaseToLatestVersion<BookingsDbContext, BookingsDbMigrationConfiguration>();
                    migrate.InitializeDatabase(context);
                }
                BookingsSeedData.Persist(context);
            }
        }
    }
}