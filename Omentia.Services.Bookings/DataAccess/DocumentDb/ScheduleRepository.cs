﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.DataAccess.DocumentDb
{
    public class ScheduleRepository : IScheduleRepository
    {
        private readonly IDocumentDatabase _db;

        public ScheduleRepository(IDocumentDatabase db)
        {
            _db = db;
        }

        public async Task<WeekSchedule> FindScheduleByDoctorAsync(Guid doctorId)
        {
            var result = await _db.Async(db => db.Query<WeekSchedule>(CollectionNames.Schedules)
                .Where(x => x.DoctorId == doctorId)
                .ToList()
                .FirstOrDefault());

            return result;
        }

        public async Task<WeekSchedule> UpsertScheduleAsync(WeekSchedule schedule)
        {
            var existing = await FindScheduleByDoctorAsync(schedule.DoctorId);
            if (existing != null)
            {
                schedule.Id = existing.Id;
            }
            await _db.Async(db => db.Upsert(CollectionNames.Schedules, schedule));
            return schedule;
        }
    }
}