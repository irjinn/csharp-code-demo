﻿namespace Omentia.Services.Bookings.DataAccess.DocumentDb
{
    public static class CollectionNames
    {
        public static string Schedules => "schedules";
    }
}