﻿using System;
using System.Linq;
using System.Reflection;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings
{
    public static partial class ModelExtensions // WeekSchedule
    {
        public static WeekSchedule SetDoctorId(this WeekSchedule schedule, Guid doctorId)
        {
            schedule.DoctorId = doctorId;
            return schedule;
        }
        public static WeekSchedule SetCareUnitId(this WeekSchedule schedule, Guid careUnitId)
        {
            schedule.CareUnitId = careUnitId;
            return schedule;
        }
        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, DayOfWeek day, string fromTime, string toTime)
            => schedule.SetWorkingTime(day, new[] { new TimeInterval(fromTime, toTime) });
        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, DayOfWeek day, TimeInterval time)
            => schedule.SetWorkingTime(day, new[] { time });

        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, DayOfWeek day, TimeInterval[] time)
        {
            var prop = GetProperty(schedule, day);
            prop.SetMethod.Invoke(schedule, new[] { (object)time });
            return schedule;
        }

        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, string fromTime, string toTime)
            => SetWorkingTime(schedule, new[] { new TimeInterval(fromTime, toTime) });
        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, TimeInterval time)
            => SetWorkingTime(schedule, new[] {time});
        public static WeekSchedule SetWorkingTime(this WeekSchedule schedule, TimeInterval[] time)
        {
            var props = GetProperties(schedule);
            foreach (var prop in props)
            {
                prop.SetMethod.Invoke(schedule, new[] {(object) time});
            }
            return schedule;
        }
        public static TimeInterval[] GetWorkingTime(this WeekSchedule schedule, DayOfWeek day)
        {
            var prop = GetProperty(schedule, day);
            return prop.GetMethod.Invoke(schedule, null) as TimeInterval[];
        }
        private static PropertyInfo GetProperty(WeekSchedule schedule, DayOfWeek day)
        {
            var prop = typeof(WeekSchedule).GetProperties().FirstOrDefault(x => x.Name == day.ToString());
            if (prop == null)
            {
                throw new ArgumentException("Unsupported day of week");
            }
            return prop;
        }
        private static PropertyInfo[] GetProperties(WeekSchedule schedule)
        {
            return typeof(WeekSchedule)
                .GetProperties()
                .Where(x => x.PropertyType == typeof(TimeInterval[]))
                .ToArray();
        }
    }
}