﻿using System;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings
{
    public static partial class ModelExtensions // Booking
    {
        /// <summary>
        /// Appends status flag to booking visit status.
        /// </summary>
        /// <param name="booking"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public static Booking SetVisitStatus(this Booking booking, VisitStatus status)
        {
            if (booking == null) return null;
            booking.VisitStatus |= status;

            return booking;
        }

        public static Booking UpdateExpiration(this Booking booking, DateTimeOffset expirationDate)
        {
            if (booking.ExpirationDate == null)
            {
                // do not set expiration if booking is not supposed to expire
                return booking;
            }
            booking.ExpirationDate = expirationDate;
            return booking;
        }

        public static bool IsPaid(this Booking booking)
            => booking.Payment != null && 
                booking.Payment.PaymentStatus != PaymentStatus.None;

        public static bool IsExpired(this Booking booking)
        {
            if (booking.ExpirationDate == null)
            {
                return false;
            }
            return booking.ExpirationDate <= DateTimeOffset.Now;
        }

        public static bool IsCancelled(this Booking booking)
            => (booking.VisitStatus & VisitStatus.Cancelled) == VisitStatus.Cancelled;

        public static Booking SetCancelled(this Booking booking)
        {
            booking.VisitStatus |= VisitStatus.Cancelled;
            return booking;
        }
    }
}