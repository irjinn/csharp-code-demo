﻿using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings
{
    public static partial class ModelExtensions // BookingFilter
    {
        public static BookingFilter IsActive(this BookingFilter filter, bool? isActive)
        {
            filter.IsActive = isActive;
            return filter;
        }
        public static BookingFilter IsCancelled(this BookingFilter filter, bool? isCancelled)
        {
            filter.IsCancelled = isCancelled;
            return filter;
        }
        public static BookingFilter HasPayment(this BookingFilter filter, bool? hasPayment)
        {
            filter.HasPayment = hasPayment;
            return filter;
        }
        public static BookingFilter HasInvoice(this BookingFilter filter, bool? hasInvoice)
        {
            filter.HasInvoice = hasInvoice;
            return filter;
        }
        public static BookingFilter IsInvoiceSent(this BookingFilter filter, bool? isInvoiceSent)
        {
            filter.IsInvoiceSent = isInvoiceSent;
            return filter;
        }
        public static BookingFilter IsExpired(this BookingFilter filter, bool? isExpired)
        {
            filter.IsExpired = isExpired;
            return filter;
        }
        public static BookingFilter HasInvoiceStatus(this BookingFilter filter, InvoiceStatus? invoiceStatus)
        {
            filter.InvoiceStatus = invoiceStatus;
            return filter;
        }
    }
}