﻿using System;
using Helios.Common.Models.Bookings;
using Helios.Common.Models.Const;

namespace Omentia.Services.Bookings
{
    public static partial class ModelExtensions // Timeslot
    {
        public static Timeslot SetStartTime(this Timeslot timeslot, DateTimeOffset dateTime)
            => SetStartTime(timeslot, dateTime, BookingDefaults.TimeslotDurationMinutes);

        public static Timeslot SetStartTime(this Timeslot timeslot, DateTimeOffset dateTime, int durationMinutes)
        {
            timeslot.Date = dateTime.Date;
            timeslot.FromDate = dateTime;
            timeslot.ToDate = dateTime.AddMinutes(durationMinutes);
            return timeslot;
        }

        public static TimeInterval GetTimeInterval(this Timeslot timeslot)
        {
            return new TimeInterval()
            {
                From = timeslot.FromDate.TimeOfDay,
                To = timeslot.ToDate.TimeOfDay,
            };
        }

        public static bool HasActualBooking(this Timeslot timeslot)
        {
            if (timeslot.ActualBooking == null || 
                timeslot.ActualBooking.IsExpired() || 
                timeslot.ActualBooking.IsCancelled())
            {
                return false;
            }
            return true;
        }

        public static Timeslot SetBooking(this Timeslot timeslot, Booking booking)
        {
            timeslot.ActualBooking = booking;
            timeslot.Bookings.Add(booking);
            booking.Timeslot = timeslot;

            return timeslot;
        }
    }
}