﻿using System;

namespace Omentia.Services.Bookings
{
    public static class DateTimeExtensions
    {
        public static bool IsHoliday(this DateTime date) => new DateTimeOffset(date).IsHoliday();
        public static bool IsHoliday(this DateTimeOffset date)
            => date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
    }
}