﻿using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings
{
    public static partial class ModelExtensions // TimeslotFilter
    {
        public static TimeslotFilter DaysAhead(this TimeslotFilter filter, int days)
        {
            filter.Days = days;
            return filter;
        }       
        public static TimeslotFilter AvailableForVideo(this TimeslotFilter filter)
        {
            filter.IsAvailableForVideo = true;
            return filter;
        }       
        public static TimeslotFilter NotBooked(this TimeslotFilter filter)
        {
            filter.IsBooked = false;
            return filter;
        }       
        public static TimeslotFilter InFuture(this TimeslotFilter filter)
        {
            filter.IsInFuture = true;
            return filter;
        }       
    }
}