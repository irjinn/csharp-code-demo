﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Helios.Common.DataAccess.Sql;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Utils;

namespace Omentia.Services.Bookings.Services
{
    public class TimetableService : ITimetableService
    {
        private readonly IScheduleRepository _scheduleRepository;
        private readonly TimeIntervalGrid _intervalGrid = new TimeIntervalGrid();

        public BookingSettings Settings { get; set; } = BookingSettings.Instance;
        public IRequestContext Context { get; }
        private IBookingsDbContext Db => Context.Sql;
        
        public TimetableService(IRequestContext context, IScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
            Context = context;
        }

        public void SeedData()
        {
            var defaultSchedule = new WeekSchedule()
                {
                    DoctorId = Settings.DemoDoctorId,
                    CareUnitId = Settings.DemoCareUnitId,
                }
                .SetWorkingTime("9:00", "14:00");

            SetDoctorSchedule(defaultSchedule);
        }

        public Timeslot CreateTimeslot(Timeslot timeslot)
        {
            timeslot.NewIdIfEmpty();
            var result = Db.Timeslots.Add(timeslot);
            return result;
        }

        public WeekSchedule GetDoctorSchedule(Guid doctorId)
            => _scheduleRepository.FindScheduleByDoctorAsync(doctorId).Result;

        public WeekSchedule SetDoctorSchedule(WeekSchedule schedule)
        {
            var doctorId = schedule.DoctorId;
            var existingTimeslots = Db.Timeslots
                .Include(x => x.ActualBooking.Payment)
                .Where(x => x.DoctorId == doctorId)
                .Where(x => x.FromDate > DateTimeOffset.Now)
                .ToList();

            var createdTimeslots = CreateTimeslots(schedule, Settings.GenerateTimeslotDaysAhead)
                .Do(x =>
                {
                    x.DoctorId = doctorId;
                    x.CareUnitId = schedule.CareUnitId;
                    x.Availability = DoctorActivity.PhysicalVisitAndVideo;
                });

            var updates = MergeTimeslots(existingTimeslots, createdTimeslots);

            var toRemove = updates.Where(x => x.Created == null).ToList();
            var toUpdate = updates.Where(x => x.Created != null && x.Existing != null).ToList();
            var toCreate = updates.Where(x => x.Existing == null);

            if (toRemove.Any(x => x.Existing.ActualBooking?.Payment?.Id != null))
            {
                //todo: decide workflow
            }
            toRemove.ForEach(x => x.Existing.Availability = DoctorActivity.Unavailable);
            toUpdate.ForEach(x => x.Existing.Availability = x.Created.Availability);
            Db.Timeslots.AddRange(toCreate.Select(x => x.Created));

            _scheduleRepository.UpsertScheduleAsync(schedule).Wait();
            return schedule;
        }

        private ICollection<TimeslotUpdate> MergeTimeslots(IEnumerable<Timeslot> existingTimeslots, IEnumerable<Timeslot> createdTimeslots)
        {
            var timeslotByDate = (existingTimeslots ?? new Timeslot[0])
                .Select(x => new TimeslotUpdate() { FromDate = x.FromDate, Existing = x })
                .ToDictionary(x => x.FromDate);

            if (createdTimeslots != null)
            {
                foreach (var timeslot in createdTimeslots)
                {
                    var update = timeslotByDate.SafeGet(timeslot.FromDate);
                    if (update != null)
                    {
                        update.Created = timeslot;
                    }
                    else
                    {
                        timeslotByDate[timeslot.FromDate] = new TimeslotUpdate() { Created = timeslot };
                    }
                }
            }
            return timeslotByDate.Values;
        }

        private IEnumerable<Timeslot> CreateTimeslots(WeekSchedule schedule, int daysAhead)
        {
            return Enumerable.Range(0, daysAhead)
                .Select(x => DateTimeOffset.Now.AddDays(x))
                .Select(x => CreateTimeslots(x, schedule.GetWorkingTime(x.DayOfWeek)))
                .Where(x => x != null)
                .SelectMany(x => x);
        }

        private IEnumerable<Timeslot> CreateTimeslots(DateTimeOffset date, TimeInterval[] workingTime)
            => workingTime?.SelectMany(x => CreateTimeslots(date, x));
        private IEnumerable<Timeslot> CreateTimeslots(DateTimeOffset date, TimeInterval workingTime)
            => _intervalGrid.SplitToGrid(workingTime)
            .Select(x => Timeslot.FromTimeInterval(date, x));

        private class TimeslotUpdate
        {
            public DateTimeOffset FromDate { get; set; }
            public Timeslot Existing { get; set; }
            public Timeslot Created { get; set; }
        }
    }
}