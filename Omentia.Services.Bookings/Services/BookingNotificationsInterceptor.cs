﻿using System.Threading.Tasks;
using Helios.Common.Http.Client;
using Helios.Common.Models.Bookings;
using Helios.Common.Notifications;

namespace Omentia.Services.Bookings.Services
{
    public class BookingNotificationsInterceptor : INotificationInterceptor
    {
        private readonly IContactsClient _contactsClient;

        public BookingNotificationsInterceptor(IContactsClient contactsClient)
        {
            _contactsClient = contactsClient;
        }
        public async Task Process(INotification notification)
        {
            var bookingNotification = notification as Notification<BookingViewModel>;
            if (bookingNotification?.Data != null)
            {
                bookingNotification.Data.Patient = await _contactsClient.WithIdAsync(bookingNotification.Data.PatientId);
            }
        }
    }
}