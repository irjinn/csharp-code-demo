﻿using System;
using System.Data.Entity;
using System.Linq;
using Helios.Common;
using Helios.Common.Models.Bookings;
using Helios.Common.Notifications;
using Helios.Common.Utils;
using Omentia.Services.Bookings.Interfaces;
using OpenTokSDK;

namespace Omentia.Services.Bookings.Services
{
    public class VideoSessionService : IVideoSessionService
    {
        private readonly INotificationService _notificationService;
        public IRequestContext Context { get; }

        private IQueryable<Booking> Bookings => Context.Sql.Bookings
            .Include(x => x.Timeslot)
            .Include(x => x.VideoSession);

        private IQueryable<VideoSession> Sessions => Context.Sql.VideoSessions
            .Include(x => x.Booking)
            .Include(x => x.Booking.Timeslot)
            .Where(x => x.Booking != null);

        public VideoSessionService(IRequestContext context, INotificationService notificationService)
        {
            Context = context;
            _notificationService = notificationService;
        }

        public  VideoSession GetOrCreateSession(Guid doctorId, Guid bookingId)
        {
            var existing = FindOpenedSession(bookingId);
            if (existing != null)
            {
                return existing;
            }
            var booking = Bookings.FirstOrDefault(x => x.Id == bookingId).EnsureNotNull(bookingId);
            if (booking.DoctorId != doctorId)
            {
                //todo: throw access violation
                throw new ValidationException($"Cannot access booking {bookingId}");
            }
            var session = CreateHostSession(bookingId);
            session.DoctorId = doctorId;
            session.PatientId = booking.PatientId;
            session.Booking = booking;
            session.PlannedEndDate = booking.Timeslot?.ToDate;

            booking.VideoSession = session;
            booking.SetVisitStatus(VisitStatus.DoctorReady);


            Context.OnCommit(
                () => _notificationService.Post(ClientNotification.VideoSessionCreated(booking)));

            return session;
        }
        
        public  VideoSession EndSession(Guid doctorId, Guid bookingId)
        {
            var session = GetOpenedSession(bookingId);
            session.ActualEndDate = DateTimeOffset.Now;
            session.Booking.SetVisitStatus(VisitStatus.Ended);

            Context.OnCommit(
                () => _notificationService.Post(ClientNotification.VideoSessionEnded(session)));

            return session;
        }

        public  VideoSession FindDoctorSession(Guid doctorId, Guid bookingId)
        {
            return FindOpenedSession(bookingId);
        }

        public  VideoSession GetPatientSession(Guid patientId, Guid bookingId)
        {
            var hostSession = FindOpenedSession(bookingId);
            hostSession?.Booking.SetVisitStatus(VisitStatus.PatientReady);

            return CreateClientSession(hostSession, patientId);
        }
        private  VideoSession GetOpenedSession(Guid bookingId)
        {
            var session = FindOpenedSession(bookingId);
            if (session.Booking == null)
            {
                //should be data corrupted exception
                throw new Helios.Common.SystemException("no booking is associated with video session"); 
            }
            if (session == null)
            {
                throw new EntityNotFoundException(
                    BookingServiceMessages.OpenedVideoSessionForBookingNotFound(bookingId));
            }
            return session;
        }

        private VideoSession FindOpenedSession(Guid bookingId)
        {
            var hostSession = Sessions.FirstOrDefault(x => x.Booking.Id == bookingId);
            if (hostSession == null ||
                hostSession.ActualEndDate != null ||
                hostSession.IsDeleted)
            {
                return null;
            }
            return hostSession;
        }

        public TimeSpan SessionTTL { get; } = TimeSpan.FromHours(4);
        public BookingSettings Settings { get; set; } = BookingSettings.Instance;

        private VideoSession CreateHostSession(Guid bookingId)
        {
            var session = new VideoSession()
            {
                ServiceApiKey = BookingSettings.Instance.TokboxApiKey.ToString()
            };

            var opentok = new OpenTok(Settings.TokboxApiKey, Settings.TokboxApiSecret);

            var nativeSession = opentok.CreateSession();
            session.ServiceSessionId = nativeSession.Id;
            session.ServiceAccessToken = opentok.GenerateToken(session.ServiceSessionId, expireTime: UnixExpirationTime());

            return session;
        }
        private double UnixExpirationTime() => DateTime.UtcNow.Add(SessionTTL).AsUnixTime();

        /// <summary>
        /// Create opentok client session. Not persisted in db (only doctor's session is persisted)
        /// </summary>
        /// <param name="session"></param>
        /// <param name="patientId"></param>
        /// <returns></returns>
        private VideoSession CreateClientSession(VideoSession session, Guid patientId)
        {
            if (session == null) return null;
            if (DateTimeOffset.Now - session.CreatedDate > SessionTTL)
            {
                return null;
            }
            var opentok = new OpenTok(Settings.TokboxApiKey, Settings.TokboxApiSecret);
            var clientSession = new VideoSession(session);
            clientSession.ServiceAccessToken = opentok.GenerateToken(clientSession.ServiceSessionId, expireTime: UnixExpirationTime());
            clientSession.PatientId = patientId;
            //do not persist client session
            clientSession.Booking = session.Booking;

            return clientSession;
        }
    }
}