﻿using System;
using System.Linq;
using Helios.Common;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.Services
{
    public class InvoiceService
    {
        public IRequestContext Context { get; }
        private IBookingsDbContext Db => Context.Sql;

        public InvoiceService(IRequestContext context)
        {
            Context = context;
        }

        private Invoice GetInvoice(Guid id)
            => Db.Invoices
                .FirstOrDefault(x => x.Id == id)
                .EnsureNotNull(id);

        public Invoice SetStatus(Guid invoiceId, InvoiceStatus status)
        {
            var invoice = GetInvoice(invoiceId);
            invoice.InvoiceStatus = status;

            return invoice.MarkAsChanged();
        }
    }
}