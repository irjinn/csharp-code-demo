﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Helios.Common;
using Helios.Common.DataAccess.Sql;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Helios.Common.Notifications;
using Omentia.Services.Bookings.DataAccess.Sql.Read;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings.Services
{
    public class BookingService : IBookingService
    {
        private readonly INotificationService _notificationService;
        public IRequestContext Context { get; }
        private IBookingsDbContext Db => Context.Sql;
        public BookingService(IRequestContext context, INotificationService notificationService)
        {
            Context = context;
            _notificationService = notificationService;
        }
        private DateTimeOffset ExpirationDate()
            => DateTimeOffset.Now.AddMinutes(BookingSettings.Instance.ExpirationIntervalMinutes);

        private IEnumerable<Booking> PatientBookings(Guid patientId, bool? isActive)
            => Db.Bookings
                .Where(x => !x.IsDeleted)
                .Where(x => x.PatientId == patientId)
                .FilterIsActive(isActive);


        public Booking GetBooking(Guid id)
            => Db.Bookings
                .Include(x => x.Timeslot)
                .Include(x => x.Payment)
                .FirstOrDefault(x => x.Id == id)
                .EnsureNotNull(id);

        public Timeslot GetTimeslot(Guid id)
            => Db.Timeslots
                .Include(x => x.ActualBooking)
                .Include(x => x.Bookings)
                .FirstOrDefault(x => x.Id == id)
                .EnsureNotNull(id);
        
        public Booking UpdateExpiration(Guid bookingId)
            => UpdateExpiration(GetBooking(bookingId));

        public Booking UpdateExpiration(Booking booking)
            => booking.UpdateExpiration(ExpirationDate()).MarkAsChanged();

        public Booking BookTimeslot(Guid patientId, Guid timeslotId, DoctorActivity activity)
        {
            var timeslot = GetTimeslot(timeslotId);
            Context.Timer.Checkpoint("get timeslot");
            Context.OnCommit(() => Context.Timer.Checkpoint("commit changes"));

            if (timeslot.HasActualBooking())
            {
                throw new ValidationException("Timeslot already booked");
            }
            if (PatientBookings(patientId, isActive: true).Any())
            {
                throw new ValidationException("Only one future visit can be booked");
            }
            
            var booking = new Booking()
            {
                PatientId = patientId,
                DoctorId = timeslot.DoctorId,
                Activity = activity,
                ExpirationDate = ExpirationDate()
            };

            timeslot.SetBooking(booking);
            return booking;
        }

        public Booking PayForBooking(Guid patientId, Guid bookingId, BookingPaymentInputModel inputModel)
        {
            var booking = GetBooking(bookingId);
            if (booking.IsPaid())
            {
                throw new ValidationException("Booking already paid");
            }
            if (booking.PatientId != patientId)
            {
                throw new UserNotAuthorizedException($"Booking with {bookingId} cannot be paid for by this user");
            }

            booking.Payment = booking.Payment ?? new Payment();
            booking.Payment.PaymentType = inputModel.Type;
            booking.Payment.PaymentStatus = PaymentStatus.Complete;

            booking.Payment.Invoice = booking.Payment.Invoice ?? new Invoice();
            booking.Payment.Invoice.InvoiceStatus = InvoiceStatus.Created;
            booking.Payment.Invoice.Address = inputModel.Address;

            // paid booking should not expire
            booking.ExpirationDate = null;

            // doctor care only about paid bookings
            var notification = ClientNotification.VisitBooked(booking);
            Context.OnCommit(() => _notificationService.Post(notification));

            return booking.MarkAsChanged();
        }

        public Booking SetBookingComment(Guid patientId, Guid bookingId, string comment)
        {
            var booking = Db.Bookings.GetExisting(bookingId);
            if (booking.PatientId != patientId)
            {
                throw new UserNotAuthorizedException($"Booking with {bookingId} cannot be modified by this user");
            }
            booking.PatientComment = comment;
            UpdateExpiration(booking);

            return booking.MarkAsChanged();
        }

        public Booking CancelBooking(Guid bookingId)
        {
            var booking = Db.Bookings
                .Include(x => x.Payment)
                .Include(x => x.Timeslot.ActualBooking)
                .FirstOrDefault(x => x.Id == bookingId)
                .EnsureNotNull(bookingId);

            if (booking.IsCancelled())
            {
                return booking;
            }

            booking.SetCancelled();
            if (booking.Timeslot?.ActualBooking?.Id == bookingId)
            {
                booking.Timeslot.ActualBooking = null;
            }
            if (booking.Payment != null) //nobody cares about booking without payment
            {
                var notification = ClientNotification.VisitCancelled(booking);
                Context.OnCommit(() => _notificationService.Post(notification));
            }

            return booking.MarkAsChanged();
        }

        public Booking DeleteBooking(Guid bookingId)
        {
            //todo: add restrictions (when old clients will be updated)
            var booking = CancelBooking(bookingId);
            return booking.SoftDelete();
        }

        //todo: remove before release
        //for development purposes only
        public int DeleteBookings(Guid doctorId)
        {
            return Db.Bookings.Clear();
        }
    }
}