﻿using System;
using System.Collections.Generic;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;

namespace Omentia.Services.Bookings.Utils
{
    public class TimeIntervalGrid
    {
        public int IntervalLength { get; } = 15;

        public IEnumerable<TimeInterval> SplitToGrid(TimeInterval interval)
            => SplitToGrid(interval, IntervalLength);

        private IEnumerable<TimeInterval> SplitToGrid(TimeInterval interval, int lengthMinutes)
        {
            var startPoint = new TimeSpan((int)Math.Floor(interval.From.TotalHours), 0, 0);
            while (startPoint < interval.From)
            {
                startPoint = startPoint.AddMinutes(lengthMinutes);
            }
            var endPoint = startPoint.AddMinutes(lengthMinutes);
            while (endPoint <= interval.To)
            {
                yield return new TimeInterval() { From = startPoint, To = endPoint };
                startPoint = endPoint;
                endPoint = endPoint.AddMinutes(lengthMinutes);
            }
        }
    }
}