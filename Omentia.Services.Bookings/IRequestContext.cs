﻿using System;
using System.Threading.Tasks;
using Helios.Common.Utils.Diagnostics;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings
{
    public interface IRequestContext : IDisposable
    {
        IBookingsDbContext Sql { get; }
        IRequestContext OnCommit(Action action);
        OperationTimer Timer { get; }
        Task CommitChangesAsync();
    }
}