﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface IBookingsDbContext : IDisposable
    {
        IDbSet<Booking> Bookings { get; set; }
        IDbSet<Timeslot> Timeslots { get; set; }
        IDbSet<VideoSession> VideoSessions { get; set; }
        IDbSet<Payment> Payments { get; set; }
        IDbSet<Invoice> Invoices { get; set; }
        Task<int> SaveChangesAsync();
    }
}