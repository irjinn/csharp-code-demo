﻿using System;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface IVideoSessionService
    {
        VideoSession GetOrCreateSession(Guid doctorId, Guid bookingId);
        VideoSession EndSession(Guid doctorId, Guid bookingId);
        VideoSession FindDoctorSession(Guid doctorId, Guid bookingId);
        VideoSession GetPatientSession(Guid patientId, Guid bookingId);
    }
}