﻿using System;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface ITimetableService
    {
        Timeslot CreateTimeslot(Timeslot timeslot);
        WeekSchedule GetDoctorSchedule(Guid doctorId);
        WeekSchedule SetDoctorSchedule(WeekSchedule schedule);
    }
}