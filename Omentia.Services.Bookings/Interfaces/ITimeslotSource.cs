﻿using System.Collections.Generic;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface ITimeslotSource
    {
        IEnumerable<Timeslot> Find(TimeslotFilter filter);
    }
}