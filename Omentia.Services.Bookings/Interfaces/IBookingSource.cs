﻿using System.Collections.Generic;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface IBookingSource
    {
        IEnumerable<Booking> Find(BookingFilter filter);
        int Count(BookingFilter filter);
    }
}