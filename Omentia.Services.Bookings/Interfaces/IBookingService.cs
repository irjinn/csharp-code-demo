﻿using System;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface IBookingService
    {
        Booking GetBooking(Guid id);
        Booking UpdateExpiration(Guid bookingId);
        Booking BookTimeslot(Guid patientId, Guid timeslotId, DoctorActivity activity);
        Booking PayForBooking(Guid patientId, Guid bookingId, BookingPaymentInputModel inputModel);
        Booking SetBookingComment(Guid patientId, Guid bookingId, string comment);
        Booking CancelBooking(Guid bookingId);
        Booking DeleteBooking(Guid bookingId);
        /// <summary>
        /// Only for development purposes
        /// </summary>
        /// <param name="doctorId"></param>
        /// <returns></returns>
        int DeleteBookings(Guid doctorId);
    }
}