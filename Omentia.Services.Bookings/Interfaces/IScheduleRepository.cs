﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Bookings;

namespace Omentia.Services.Bookings.Interfaces
{
    public interface IScheduleRepository
    {
        Task<WeekSchedule> FindScheduleByDoctorAsync(Guid doctorId);
        Task<WeekSchedule> UpsertScheduleAsync(WeekSchedule schedule);
    }
}