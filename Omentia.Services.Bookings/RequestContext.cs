﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Helios.Common;
using Helios.Common.Utils.Diagnostics;
using Omentia.Services.Bookings.DataAccess.Sql;
using Omentia.Services.Bookings.Interfaces;

namespace Omentia.Services.Bookings
{
    public class RequestContext : IRequestContext
    {
        private readonly ConcurrentQueue<Action> _onCommit = new ConcurrentQueue<Action>();
        public Guid Id { get; set; } = GuidId.New();
        public IBookingsDbContext Sql { get; }
        public OperationTimer Timer { get; } = new OperationTimer();

        public RequestContext()
        {
            Sql = new BookingsDbContext();
        }

        public IRequestContext OnCommit(Action action)
        {
            _onCommit.Enqueue(action);
            return this;
        }

        public async Task CommitChangesAsync()
        {
            await Sql.SaveChangesAsync().ConfigureAwait(false);
            Action action;
            while (_onCommit.TryDequeue(out action))
            {
                action();
            }
        }
        
        public bool IsDisposed { get; private set; }
        public void Dispose()
        {
            if (IsDisposed)
            {
                throw new ObjectDisposedException(nameof(RequestContext));
            }
            IsDisposed = true;

            Timer.Dispose();
            Sql.Dispose();
        }
    }
}