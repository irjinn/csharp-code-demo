﻿using System;
using Helios.Common;
using Helios.Common.Infrastructure.Settings;

namespace Omentia.Services.Bookings
{
    public class BookingSettings
    {
        private static BookingSettings _instance;
        public static BookingSettings Instance
        {
            get
            {
                if (_instance == null)
                {
                    throw new ApplicationConfigurationException("Settings is not initialized");
                }
                return _instance;
            }
            set { _instance = value; }
        }

        public int TokboxApiKey { get; set; }
        public string TokboxApiSecret { get; set; }

        /// <summary>
        /// Do not return timeslots that starts sooner than this interval from now
        /// </summary>
        public int BookingDeadlineMinutes { get; set; }

        public int ExpirationIntervalMinutes { get; set; }

        public int GenerateTimeslotDaysAhead { get; set; }

        public Guid DemoDoctorId { get; set; }

        public Guid DemoCareUnitId { get; set; }

        public static void Initialize(ISettingsSource source)
        {
            Instance = new BookingSettings()
            {
                TokboxApiKey = source.GetValue<int>(SettingsKeys.Booking.TokboxApiKey),
                TokboxApiSecret = source.GetValue<string>(SettingsKeys.Booking.TokboxApiSecret),
                BookingDeadlineMinutes = source.GetValue<int>(SettingsKeys.Booking.BookingDeadlineMinutes),
                ExpirationIntervalMinutes = source.GetValue<int>(SettingsKeys.Booking.ExpirationIntervalMinutes),
                GenerateTimeslotDaysAhead = source.GetValue<int>(SettingsKeys.Booking.GenerateTimeslotDaysAhead),

                DemoDoctorId = source.GetValue<Guid>(SettingsKeys.Booking.DemoDoctorId, new Guid("321b4f7b-e460-45c7-831f-d1f867e10d32")),
                DemoCareUnitId = source.GetValue<Guid>(SettingsKeys.Booking.DemoCareUnitId, new Guid("3ccd46eb-4cc8-4085-9934-f3645b43c783")),
            };
        }
    }
}