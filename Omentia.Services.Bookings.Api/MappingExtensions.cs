﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Http.Client;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;

namespace Omentia.Services.Bookings.Api
{
    /// <summary>
    /// Response customizations.
    /// </summary>
    public static class MappingExtensions
    {
        /// <summary>
        /// Batch inject doctors as contacts from external source.
        /// </summary>
        public static async Task<ICollection<TimeslotViewModel>> SetDoctorsAsync(this ICollection<TimeslotViewModel> viewModels,
            IContactsClient contactsClient)
        {
            var vms = viewModels?.Where(x => x.DoctorId != null).ToArray();
            if (vms == null || !vms.Any())
            {
                return viewModels;
            }

            // ReSharper disable once PossibleInvalidOperationException
            var ids = vms.Select(x => (Guid)x.DoctorId).ToArray();
            var contacts = await contactsClient.WithIdsAsync(ids);
            var contactsById = contacts.ToDictionary(x => x.Id);
            vms.ForEach(x => x.Doctor = contactsById.SafeGet(x.DoctorId));

            return viewModels;
        }
        /// <summary>
        /// Batch inject patients as contacts from external source.
        /// </summary>
        public static async Task<ICollection<BookingViewModel>> SetPatientsAsync(this ICollection<BookingViewModel> viewModels,
            IContactsClient contactsClient)
        {
            if (viewModels == null || !viewModels.Any())
            {
                return viewModels;
            }
            var ids = viewModels.Select(x => x.PatientId).ToArray();
            var contacts = await contactsClient.WithIdsAsync(ids);
            var contactsById = contacts.ToDictionary(x => x.Id);
            viewModels.ForEach(x => x.Patient = contactsById.SafeGet(x.PatientId));

            return viewModels;
        }
        /// <summary>
        /// Batch inject patients names from external source.
        /// </summary>
        public static async Task<ICollection<InvoiceViewModel>> SetContactsAsync(this ICollection<InvoiceViewModel> viewModels,
            IContactsClient contactsClient)
        {
            if (viewModels == null || !viewModels.Any())
            {
                return viewModels;
            }
            var ids = viewModels.Select(x => x.PatientId)
                .Concat(viewModels.Select(x => x.DoctorId))
                .Distinct()
                .ToArray();

            var contacts = await contactsClient.WithIdsAsync(ids);
            var contactsById = contacts.ToDictionary(x => x.Id);
            viewModels.ForEach(x =>
            {
                x.Patient = contactsById.SafeGet(x.PatientId);
                x.Doctor = contactsById.SafeGet(x.DoctorId);
            });

            return viewModels;
        }
        /// <summary>
        /// Inject doctor as contacts from external source.
        /// </summary>
        public static async Task<TimeslotViewModel> SetDoctorAsync(this TimeslotViewModel viewModel,
            IContactsClient contactsClient)
        {
            if (viewModel == null || contactsClient == null || viewModel.DoctorId.IsEmpty())
            {
                return viewModel;
            }
            // ReSharper disable once PossibleInvalidOperationException
            var contact = await contactsClient.WithIdAsync(viewModel.DoctorId.Value);
            viewModel.Doctor = contact;
            return viewModel;
        }
        /// <summary>
        /// Inject patient as contacts from external source.
        /// </summary>
        public static async Task<BookingViewModel> SetPatientAsync(this BookingViewModel viewModel,
            IContactsClient contactsClient)
        {
            if (viewModel == null || contactsClient == null || viewModel.PatientId.IsEmpty())
            {
                return viewModel;
            }

            var contact = await contactsClient.WithIdAsync(viewModel.PatientId);
            viewModel.Patient = contact;
            return viewModel;
        }
    }
}