﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Bookings.Interfaces;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Bookings.Api.Controllers
{
    /// <summary>
    /// VideoSessions for patients.
    /// </summary>
    public class PatientVideoController : ControllerBase
    {
#pragma warning disable 1591
        public IVideoSessionService SessionService { get; set; }
        public IRequestContext Context { get; set; }
#pragma warning restore 1591

        /// <summary>
        /// Get video session for booking (if doctor created one).
        /// </summary>
        [HttpGet]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}/videosession")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<VideoSession>))]
        public async Task<HttpResponseMessage> GetSession(Guid patientId, Guid bookingId)
        {
            var session = await SessionService.Async(x => x.GetPatientSession(patientId, bookingId));

            //todo: force clients to use post or put to explicit change state to 'patient is ready'
            await Context.CommitChangesAsync();

            return Success(session);
        }
    }
}