﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Http.Client;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Bookings.Interfaces;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Bookings.Api.Controllers
{
    /// <summary>
    /// Booking operations for patients.
    /// </summary>
    [RoutePrefix("")]
    public class PatientController : ControllerBase
    {
#pragma warning disable 1591
        public IRequestContext Context { get; set; }

        public IBookingService BookingService { get; set; }
        public IContactsClient ContactsClient { get; set; }
        public ITimeslotSource TimeslotSource { get; set; }
        public IBookingSource BookingSource { get; set; }
#pragma warning restore 1591

        /// <summary>
        /// Get available timeslots for doctor.
        /// </summary>
        [HttpGet]
        [Route("doctors/{doctorId:guid}/timeslots")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<TimeslotViewModel[]>))]
        public async Task<HttpResponseMessage> GetDoctorTimeslots(Guid doctorId, int days = 7)
        {
            var filter = TimeslotFilter.WithDoctor(doctorId)
                .InFuture()
                .DaysAhead(days)
                .AvailableForVideo()
                .NotBooked();

            var result = await TimeslotSource.Async(x => x.Find(filter).ToList());
            var viewModels = result.MapToArray(Map.Timeslots.AsViewModel);
            return Success(viewModels);
        }

        /// <summary>
        /// Get my bookings.
        /// </summary>
        /// <param name="patientId">Patient id.</param>
        /// <param name="active">Upcoming bookings or history/cancelled.</param>
        [HttpGet]
        [Route("patients/{patientId:guid}/bookings")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel[]>))]
        public async Task<HttpResponseMessage> GetBookings(Guid patientId, bool? active = null)
        {
            var filter = BookingFilter.WithPatient(patientId)
                .IsExpired(false)
                .IsActive(active);

            var result = await BookingSource.Async(x => x.Find(filter).ToList());

            Context.Timer.Checkpoint("load bookings");
            var viewModels = result.MapToArray(Map.Bookings.AsViewModel);

            await viewModels.Select(x => x.Timeslot).ToList().SetDoctorsAsync(ContactsClient);
            Context.Timer.Checkpoint("load contacts");

            return Success(viewModels);
        }
        
        /// <summary>
        /// Book timeslot for videocall.
        /// </summary>
        [HttpPost]
        [Route("patients/{patientId:guid}/bookings")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> BookVideoCall(Guid patientId, CreateBookingInputModel inputModel)
        {
            var activity = Map.Bookings.AsDoctorActivity(inputModel.BookingReason);
            var result = await BookingService.Async(x => x.BookTimeslot(patientId, inputModel.TimeslotId, activity));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }

        /// <summary>
        /// Prolongate booking expiration.
        /// </summary>
        [HttpPost]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}/reservation")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<DateTimeOffset?>))]
        public async Task<HttpResponseMessage> Reservation(Guid patientId, Guid bookingId)
        {
            var booking = await BookingService.Async(x => x.UpdateExpiration(bookingId));
            await Context.CommitChangesAsync();

            return Success(booking.ExpirationDate);
        }

        /// <summary>
        /// Commit payment for booking.
        /// </summary>
        [HttpPost]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}/payment")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> PayForBooking(Guid patientId, Guid bookingId, BookingPaymentInputModel inputModel)
        {
            var result = await BookingService.Async(x => x.PayForBooking(patientId, bookingId, inputModel));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }
        
        /// <summary>
        /// Add or update booking comment.
        /// </summary>
        [HttpPost]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}/comment")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> CommentBooking(Guid patientId, Guid bookingId, [FromBody] string comment)
        {
            var result = await BookingService.Async(x => x.SetBookingComment(patientId, bookingId, comment));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }

        /// <summary>
        /// Cancel booking. 
        /// </summary>
        [HttpPost]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}/status/cancelled")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> CancelBooking(Guid patientId, Guid bookingId)
        {
            var result = await BookingService.Async(x => x.CancelBooking(bookingId));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }

        /// <summary>
        /// Delete booking.
        /// </summary>
        [HttpDelete]
        [Route("patients/{patientId:guid}/bookings/{bookingId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> DeleteBooking(Guid patientId, Guid bookingId)
        {
            var result = await BookingService.Async(x => x.DeleteBooking(bookingId));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }
    }
}