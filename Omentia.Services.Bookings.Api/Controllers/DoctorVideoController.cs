﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Bookings.Interfaces;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Bookings.Api.Controllers
{
    /// <summary>
    /// VideoSessions for care-givers
    /// </summary>
    public class DoctorVideoController : ControllerBase
    {
#pragma warning disable 1591
        public IRequestContext Context { get; set; }
        public IVideoSessionService SessionService { get; set; }
#pragma warning restore 1591

        /// <summary>
        /// Get existed session for booking if already created.
        /// </summary>
        [HttpGet]
        [Route("doctors/{doctorId:guid}/bookings/{bookingId:guid}/videosession")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<VideoSession>))]
        public async Task<HttpResponseMessage> GetSession(Guid doctorId, Guid bookingId)
        {
            var session = await SessionService.Async(x => x.FindDoctorSession(doctorId, bookingId));
            return Success(session);
        }

        /// <summary>
        /// Get or create video session for booking.
        /// </summary>
        [HttpPost]
        [Route("doctors/{doctorId:guid}/bookings/{bookingId:guid}/videosession")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<VideoSession>))]
        public async Task<HttpResponseMessage> GetOrCreateSession(Guid doctorId, Guid bookingId)
        {
            var session = await SessionService.Async(x => x.GetOrCreateSession(doctorId, bookingId));
            await Context.CommitChangesAsync();

            return Success(session);
        }

        /// <summary>
        /// End video call.
        /// </summary>
        [HttpDelete]
        [Route("doctors/{doctorId:guid}/bookings/{bookingId:guid}/videosession")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<VideoSession>))]
        public async Task<HttpResponseMessage> DeleteSession(Guid doctorId, Guid bookingId)
        {
            await SessionService.Async(x => x.EndSession(doctorId, bookingId));
            await Context.CommitChangesAsync();

            return Success(bookingId);
        }
    }
}