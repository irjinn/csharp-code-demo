﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Http.Client;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Bookings.Interfaces;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Bookings.Api.Controllers
{
    /// <summary>
    /// Booking operations for care-givers
    /// </summary>
    [RoutePrefix("")]
    public class DoctorController : ControllerBase
    {
        #pragma warning disable 1591

        public IRequestContext Context { get; set; }

        public IContactsClient ContactsClient { get; set; }
        public IBookingSource BookingSource { get; set; }
        public ITimetableService TimetableService { get; set; }
        public IBookingService BookingService { get; set; }

        #pragma warning restore 1591

        /// <summary>
        /// Get bookings for specific doctor.
        /// </summary>
        [HttpGet]
        [Route("doctors/{doctorId:guid}/bookings")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel[]>))]
        public async Task<HttpResponseMessage> GetBookings(Guid doctorId, bool? actual = null)
        {
            var filter = BookingFilter.WithDoctor(doctorId)
                .IsActive(actual)
                .HasPayment(true);

            var result = await BookingSource.Async(x => x.Find(filter).ToList());
            var viewModels = result.MapToArray(Map.Bookings.AsViewModel);

            await viewModels.SetPatientsAsync(ContactsClient);
            return Success(viewModels);
        }
        
        /// <summary>
        /// Cancel booking.
        /// </summary>
        [HttpPost]
        [Route("doctors/{doctorId:guid}/bookings/{bookingId:guid}/status/cancelled")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<BookingViewModel>))]
        public async Task<HttpResponseMessage> CancelBooking(Guid doctorId, Guid bookingId)
        {
            var result = await BookingService.Async(x => x.CancelBooking(bookingId));
            await Context.CommitChangesAsync();

            var viewModel = Map.Bookings.AsViewModel(result);
            return Success(viewModel);
        }

        /// <summary>
        /// For demo use only. Delete all bookings.
        /// </summary>
        [HttpDelete]
        [Route("doctors/{doctorId:guid}/bookings")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<int>))]
        public async Task<HttpResponseMessage> DeleteBookings(Guid doctorId)
        {
            var count = await BookingService.Async(x => x.DeleteBookings(doctorId));
            await Context.CommitChangesAsync();

            return Success(count);
        }

        /// <summary>
        /// Get doctor timetable.
        /// </summary>
        [HttpGet]
        [Route("doctors/{doctorId:guid}/schedule/weekly")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<WeekScheduleViewModel>))]
        public async Task<HttpResponseMessage> GetDoctorShedule(Guid doctorId)
        {
            //todo: accept care unit id
            var result = await TimetableService.Async(x => x.GetDoctorSchedule(doctorId));
            var viewModel = Map.Schedule.AsViewModel(result);
            return Success(viewModel);
        }

        /// <summary>
        /// Update doctor timetable.
        /// </summary>
        [HttpPut]
        [Route("doctors/{doctorId:guid}/schedule/weekly")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<WeekScheduleViewModel>))]
        public async Task<HttpResponseMessage> SetDoctorShedule(Guid doctorId, WeekScheduleViewModel schedule)
        {
            //todo: accept care unit id
            var model = Map.Schedule.AsModel(schedule).SetDoctorId(doctorId)
                .SetCareUnitId(BookingSettings.Instance.DemoCareUnitId);

            var result = await TimetableService.Async(x => x.SetDoctorSchedule(model));
            await Context.CommitChangesAsync();

            var viewModel = Map.Schedule.AsViewModel(result);
            return Success(viewModel);
        }
    }
}