﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Http.Client;
using Helios.Common.Models;
using Helios.Common.Models.Bookings;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Services;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Bookings.Api.Controllers
{
    /// <summary>
    /// Operations for administrators
    /// </summary>
    public class AdminController : ControllerBase
    {
        #pragma warning disable 1591

        public IRequestContext Context { get; set; }

        public IContactsClient ContactsClient { get; set; }
        public IBookingSource BookingSource { get; set; }
        public InvoiceService InvoiceService { get; set; }

#pragma warning restore 1591
        
        /// <summary>
        /// Get all invoices.
        /// </summary>
        [HttpGet]
        [Route("invoices")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<InvoiceViewModel[]>))]
        public async Task<HttpResponseMessage> GetInvoices(bool? sent = null)
        {
            var filter = BookingFilter.New()
                .IsCancelled(false)
                .HasInvoice(true)
                .IsInvoiceSent(sent);

            var bookings = await BookingSource.Async(x => x.Find(filter));
            var viewModels = bookings.MapToArray(Map.Invoices.AsViewModel);
            await viewModels.SetContactsAsync(ContactsClient);
            return Success(viewModels);
        }

        /// <summary>
        /// Set invoice status.
        /// </summary>
        [HttpPost]
        [Route("invoices/{invoiceId:guid}/status/{status}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<Guid>))]
        public async Task<HttpResponseMessage> SetInvoiceStatus(Guid invoiceId, InvoiceStatus status)
        {
            await InvoiceService.Async(x => x.SetStatus(invoiceId, status));
            await Context.CommitChangesAsync();
            return Success(invoiceId);
        }

        /// <summary>
        /// Count of invoices with specific status.
        /// </summary>
        [HttpGet]
        [Route("invoices/{status}/count")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<int>))]
        public async Task<HttpResponseMessage> IvoiceCount(InvoiceStatus status)
        {
            var filter = BookingFilter.New()
                .IsCancelled(false)
                .HasInvoice(true)
                .HasInvoiceStatus(status);

            var count = await BookingSource.Async(x => x.Count(filter));
            return Success(count);
        }
    }
}