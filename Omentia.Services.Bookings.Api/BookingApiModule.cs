﻿using Autofac;
using Helios.Common.Infrastructure;
using Helios.Common.Notifications;
using Omentia.Services.Bookings.DataAccess.DocumentDb;
using Omentia.Services.Bookings.DataAccess.Sql;
using Omentia.Services.Bookings.DataAccess.Sql.Read;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Services;

namespace Omentia.Services.Bookings.Api
{
    /// <summary>
    /// Booking application registrations
    /// </summary>
    public class BookingApiModule : Module
    {
        /// <inheritdoc />
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<HttpClientModule>(); // for IContactsClient
            builder.RegisterModule<NotificationsModule>(); //INotificationService

            // controller context
            builder.RegisterType<RequestContext>().As<IRequestContext>().InstancePerLifetimeScope();

            // services
            builder.RegisterType<TimetableService>().As<ITimetableService>();
            builder.RegisterType<BookingService>().As<IBookingService>();
            builder.RegisterType<VideoSessionService>().As<IVideoSessionService>();
            builder.RegisterType<InvoiceService>().AsSelf();
            builder.RegisterType<BookingNotificationsInterceptor>().As<INotificationInterceptor>();

            // documentDb
            builder.RegisterModule<DocumentDbModule>();
            builder.RegisterType<ScheduleRepository>().As<IScheduleRepository>().SingleInstance();

            // sql
            builder.RegisterType<BookingsDbContext>().As<IBookingsDbContext>().InstancePerLifetimeScope();
            builder.RegisterType<BookingSource>().As<IBookingSource>();
            builder.RegisterType<TimeslotSource>().As<ITimeslotSource>();
        }
    }
}