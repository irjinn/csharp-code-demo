﻿using System;
using System.Web;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Helios.Common;
using Helios.Common.Infrastructure;
using Helios.Common.Infrastructure.Settings;
using Omentia.Common.Web.Swagger;
using Omentia.Services.Bookings.Api.Utils;
using Omentia.Services.Bookings.DataAccess.Sql;

namespace Omentia.Services.Bookings.Api
{
    /// <summary>
    /// Booking web api application.
    /// </summary>
    public class WebApiApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Entry point for application.
        /// </summary>
        protected void Application_Start()
        {
            LogConfiguration.Initialize();
            Log.Debug("application starting");
            this.Error += OnError;

            BookingSettings.Initialize(AppEnvironment.Config);
            BookingsDb.Initialize(AppEnvironment.Sql.AutoMigrate);

            AppContainer.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(AppContainer.Instance);

            GlobalConfiguration.Configuration.MessageHandlers.Add(new RequestTimingLogHandler());
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(c =>
                SwaggerConfig.RegisterDefault(c, "Booking Api", "Omentia.Services.Bookings.Api.xml"));

            Log.Info("application started");
        }

        /// <summary>
        /// Log unhandled exceptions 
        /// </summary>
        private void OnError(object sender, EventArgs args)
        {
            var prop = sender?.GetType().GetProperty("Context"); //weird hack for asp.web api
            var context = prop?.GetValue(sender) as HttpContext;

            Log.Error(message: "unhandled exception occured", data: args, exception: context?.Error);
        }
    }
}
