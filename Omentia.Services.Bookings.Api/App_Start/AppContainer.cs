﻿using System.Linq;
using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Services;

namespace Omentia.Services.Bookings.Api
{
    /// <summary>
    /// Static autofac application container wrapper.
    /// </summary>
    public class AppContainer
    {
        /// <summary>
        /// Application DI container instance.
        /// </summary>
        public static IContainer Instance { get; private set; }

        /// <summary>
        /// Make registrations and build app container.
        /// </summary>
        public static void Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            builder.RegisterModule(new BookingApiModule());

            Instance = builder.Build();
            SeedData(Instance);
        }

        /// <summary>
        /// Temporary hack for demo data initialization.
        /// </summary>
        /// <param name="container"></param>
        public static void SeedData(IContainer container)
        {
            var context = container.Resolve<IRequestContext>();
            if (context == null || context.Sql.Timeslots.Any()) return;

            var timetable = container.Resolve<ITimetableService>() as TimetableService;
            timetable?.SeedData();
            context.CommitChangesAsync().Wait(10000);
        }
    }
}