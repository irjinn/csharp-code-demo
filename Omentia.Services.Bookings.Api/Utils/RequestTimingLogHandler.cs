﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Helios.Common.Infrastructure;

namespace Omentia.Services.Bookings.Api.Utils
{
    //todo: extract to common code, make generic, accept TRequestContext type parameter
    public class RequestTimingLogHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request.RequestUri.AbsolutePath.Contains("/swagger/"))
            {
                return await base.SendAsync(request, cancellationToken);
            }

            var response = await base.SendAsync(request, cancellationToken);

            var scope = request.GetDependencyScope();
            var context = scope?.GetService(typeof(IRequestContext)) as IRequestContext;
            Log.Request(request.Method, request.RequestUri.ToString(), context?.Timer);

            return response;
        }
    }
}