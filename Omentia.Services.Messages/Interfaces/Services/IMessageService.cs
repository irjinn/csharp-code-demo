﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessageService
    {
        Task<Message> SendDoctorMessageAsync(Message message);
        Task<Message> SendPatientMessageAsync(Message message);
        Task SetMessageIsRead(Guid messageId);
    }
}