﻿using System.Collections.Generic;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessageTagsParser
    {
        IEnumerable<string> ParseMessage(string message);
    }
}