﻿using FluentValidation;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessageValidator : IValidator<Message>
    {
        
    }
}