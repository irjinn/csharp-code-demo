﻿using System;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessageRepository : IRepository<Message, Guid>
    {
    }
}
