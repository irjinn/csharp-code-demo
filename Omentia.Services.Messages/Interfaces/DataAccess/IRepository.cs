﻿using System.Threading.Tasks;

namespace Omentia.Services.Messages.Interfaces
{

    public interface IRepository<T, in TKey>
    {
        Task<T> GetAsync(TKey key);
        Task<T> AddAsync(T entity);
        Task<T> UpdateAsync(T entity);
        Task<T> DeleteAsync(TKey key);
    }
}