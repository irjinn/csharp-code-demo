﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessageStorageFacade
    {
        Task<Message> AddMessageAsync(Message message);
        Task<Message> SetMessageIsReadAsync(Guid messageId);

        Task<Conversation> FindConversationAsync(Guid? conversationId, Guid doctorId, Guid patientId);
        Task<Conversation> UpsertConversationAsync(Conversation conversation);

        Task<Contact> GetContactAsync(Guid id);
    }
}