﻿using System;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IMessagingCacheFacade
    {
        Message MessageById(Guid id);
        Contact ContactById(Guid id);
        Contact ContactByName(string name);

        Message Upsert(Message message);
        Contact Upsert(Contact contact);
        void Reset();
    }
}