﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Contacts;

namespace Omentia.Services.Messages.Interfaces
{
    [Obsolete("Use IContactsClient instead")]
    public interface IContactSource
    {
        Task<Contact> FindByIdAsync(Guid id);
        Task<Contact> FindByNameAsync(string name);
        Task<Contact[]> DoctorsAsync();
        Task<Contact[]> PatientsAsync();
    }
}