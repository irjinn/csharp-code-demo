﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Interfaces
{
    public interface IConversationSource
    {
        Task<Conversation> Details(Guid conversationId);
        Task<Conversation> Details(Guid doctorId, Guid patientId);
        Task<Conversation> FindOne(Guid doctorId, Guid patientId);
        Task<int> TotalUnread(Guid patientId);
        Task<Conversation[]> Filter(ConversationFilter filter);
    }
}