﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.TagsParsers
{
    public sealed class SampleTagsParser : IMessageTagsParser
    {
        public static readonly Dictionary<string, string> TagsStrings =
            new Dictionary<string, string>
            {
                {Tags.RecipeRenewal, "recept|förnya|medicin|förnyelse|mediciner"},
                {Tags.LabResults, "labb|labbsvar|resultat|prov|provsvar|test"},
            };

        public static readonly RegexOptions RegexOptions = RegexOptions.Compiled |
                                                           RegexOptions.CultureInvariant |
                                                           RegexOptions.IgnorePatternWhitespace |
                                                           RegexOptions.IgnoreCase;

        private static readonly Dictionary<string, Regex> RegexByTag = TagsStrings.ToDictionary(key => key.Key,
            value => new Regex(value.Value, RegexOptions));

        public class Tags
        {
            public const string Other = "Övrigt";
            public const string RecipeRenewal = "Receptförnyelse";
            public const string LabResults = "Labbsvar";
        }

        public IEnumerable<string> ParseMessage(string message)
        {
            if (message.IsNullOrEmpty())
            {
                yield break;
            }
            var match = false;
            foreach (var regex in RegexByTag)
            {
                if (!regex.Value.IsMatch(message))
                    continue;
                match = true;
                yield return regex.Key;
            }
            if (!match)
                yield return Tags.Other;
        }
    }
}
