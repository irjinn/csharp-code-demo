﻿using System;

namespace Omentia.Services.Messages.Services
{
    public static class MessageServiceTextBuilder
    {
        public static string CannotFindConversationWithPatient(Guid patientId) =>
            string.Format(MessageServiceRes.NotFoundConversationWithPatient, patientId);

        public static string CannotSendMessageToConversation(Guid conversationId) =>
            string.Format(MessageServiceRes.MessageCannotBeSentToConversation, conversationId);

        public static string CannotFindCareUnit(Guid careUnitId) =>
            string.Format(MessageServiceRes.CareUnitNotFound, careUnitId);
    }
}