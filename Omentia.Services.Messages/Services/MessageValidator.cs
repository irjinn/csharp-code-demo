﻿using System;
using FluentValidation;
using Helios.Common.Models.Messages;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.Services
{
    public sealed class MessageValidator : AbstractValidator<Message>, IMessageValidator
    {
        public MessageValidator()
        {
            RuleFor(m => m.Body)
                .NotEmpty()
                .WithLocalizedMessage(() => MessageServiceRes.MessageBodyCannotBeEmpty);

            RuleFor(m => m.FromId)
                .NotEmpty()
                .NotEqual(Guid.Empty)
                .WithLocalizedMessage(() => MessageServiceRes.MessageFromIdCannotBeEmpty);

            RuleFor(m => m.ToId)
                .NotEmpty()
                .NotEqual(Guid.Empty)
                .WithLocalizedMessage(() => MessageServiceRes.MessageToIdCannotBeEmpty);
        }
    }
}