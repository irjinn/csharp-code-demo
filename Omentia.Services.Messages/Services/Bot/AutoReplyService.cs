﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Messages;
using Omentia.Services.Messages.Extensions;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.Services
{
    public class AutoReplyService : IAutoReplyService
    {
        private readonly IMessageService _service;
        private readonly IMessageStorageFacade _storage;

        public AutoReplyService(IMessageService service, IMessageStorageFacade storage)
        {
            _service = service;
            _storage = storage;
        }

        public Task SendMessageAsync(Message message, int delaySeconds)
            => Task.Delay(TimeSpan.FromSeconds(delaySeconds))
                .ContinueWith(_ => SendMessageAsync(message));

        public async Task SendMessageAsync(Message message)
        {
            message.From = message.From ?? await _storage.GetContactAsync(message.FromId);
            if (message.From.IsPatient())
            {
                await _service.SendPatientMessageAsync(message);
            }
            else
            {
                await _service.SendDoctorMessageAsync(message);
            }
        }

        public Task ReadMessageAsync(Message message, int delaySeconds)
            => Task.Delay(TimeSpan.FromSeconds(delaySeconds))
                .ContinueWith(_ => ReadMessageAsync(message));

        public Task ReadMessageAsync(Message message)
            => _service.SetMessageIsRead(message.Id);
    }
}