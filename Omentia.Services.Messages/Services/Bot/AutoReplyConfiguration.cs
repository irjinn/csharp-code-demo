﻿using System;
using System.Collections.Generic;

namespace Omentia.Services.Messages.Services
{
    public class AutoReplyConfiguration
    {
        public List<Guid> Listeners { get; set; } = new List<Guid>();
        public int MessageReadDelay { get; set; }
        public int MessageReplyDelay { get; set; }
    }
}