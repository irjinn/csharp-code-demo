﻿using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Helios.Common.Http.Client;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Extensions;

namespace Omentia.Services.Messages.Services
{
    public class AutoReplyBot
    {
        private readonly AutoReplyConfiguration _configuration;
        private readonly IAutoReplyService _service;
        private readonly IContactsClient _contactsClient;

        public AutoReplyBot(AutoReplyConfiguration configuration, IAutoReplyService service, IContactsClient contactsClient)
        {
            _configuration = configuration;
            _service = service;
            _contactsClient = contactsClient;
        }

        public async Task HandleMessage(Message msg)
        {
            if (!_configuration.Listeners.Contains(msg.ToId))
            {
                return;
            }

            _service.ReadMessageAsync(msg, _configuration.MessageReadDelay).FireAndForget();

            await Task.FromResult(0);
            //var responseMessage = msg.CreateReply().SetBody($"Reply to ({Quote(msg.Body)}...)");
            var responseMessage = await CreateReplyAsync(msg);
            if (responseMessage != null)
            {
                _service.SendMessageAsync(responseMessage, _configuration.MessageReplyDelay).FireAndForget();
            }
        }

        private async Task<Message> CreateReplyAsync(Message msg)
        {
            msg.From = msg.From ?? await _contactsClient.WithIdAsync(msg.FromId);
            msg.To = msg.To ?? await _contactsClient.WithIdAsync(msg.ToId);
            if (!msg.IsBetweenDoctorAndPatient())
            {
                return null;
            }
            var reply = msg.CreateReply()
                .SetBody($"Reply to ({Quote(msg.Body)})");
            
            if (msg.Body.IsNullOrEmpty())
            {
                return reply;
            }

            //send response from another user
            var contactReference = GetContactReference(msg.Body);
            if (contactReference != null)
            {
                Guid id;
                Contact contact;
                if (Guid.TryParse(contactReference, out id))
                {
                    contact = await _contactsClient.WithIdAsync(id);
                }
                else
                {
                    //todo: implement find contact by name endpoint
                    contact = null; 
                }
                if (contact != null && contact.IsPatient() == reply.From.IsPatient())
                {
                    reply.SetFrom(contact);
                    reply.SetBody(CreateMessageTo(reply.To));
                }
            }
            return reply;
        }

        private readonly Regex _userReferenceRegex 
            = new Regex(@"^\W*@\W*(.+)\W*", RegexOptions.IgnoreCase);
        private string GetContactReference(string text)
        {
            var result = _userReferenceRegex.Match(text);
            return result.Groups.Count > 1
                ? result.Groups[1].Value
                : null;
        }
        
        private string Quote(string message)
        {
            if (message.IsEmpty()) return "''";
            var length = message.Length;
            if (length < 20) return message;
            return message.Substring(0, Math.Min(length, 20)) + "...";
        }

        private string CreateMessageTo(Contact to)
        {
            return to.IsPatient()
                ? $"Greetings, mr. {to.Name}. Do you feel well?"
                : $"Hello, dr. {to.Name}. I need more medication.";
        }
    }
}