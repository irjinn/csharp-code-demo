﻿using System.Threading.Tasks;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Services
{
    public interface IAutoReplyService
    {
        Task ReadMessageAsync(Message message);
        Task ReadMessageAsync(Message message, int delaySeconds);
        Task SendMessageAsync(Message message);
        Task SendMessageAsync(Message message, int delaySeconds);
    }
}