﻿using System;
using System.Threading.Tasks;
using Helios.Common.Infrastructure.Extensions;
using Helios.Common.Models;
using Helios.Common.Models.Messages;
using Helios.Common.Notifications;
using JetBrains.Annotations;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.Services
{
    public sealed class MessageService : IMessageService
    {
        private readonly IMessageValidator _messageValidator;
        private readonly IMessageStorageFacade _storage;
        private readonly INotificationService _notificationService;

        public MessageService(
            [NotNull] IMessageValidator messageValidator,
            [NotNull] IMessageStorageFacade storage,
            [NotNull] INotificationService notificationService)
        {
            if (messageValidator == null) throw new ArgumentNullException(nameof(messageValidator));
            if (storage == null) throw new ArgumentNullException(nameof(storage));
            if (notificationService == null) throw new ArgumentNullException(nameof(notificationService));

            _messageValidator = messageValidator;
            _storage = storage;
            _notificationService = notificationService;
        }

        public Task<Message> SendPatientMessageAsync(Message message)
            => SendMessageAsync(message, doctorId: message.ToId, patientId: message.FromId);

        public Task<Message> SendDoctorMessageAsync(Message message)
            => SendMessageAsync(message, doctorId: message.FromId, patientId: message.ToId);

        public async Task SetMessageIsRead(Guid messageId)
        {
            var message = await _storage.SetMessageIsReadAsync(messageId);
            _notificationService.Post(ClientNotification.MessageRead(message));
        }
        
        private async Task<Message> SendMessageAsync(Message message, Guid doctorId, Guid patientId)
        {
            await _messageValidator.PerformValidation(message);

            message.CreatedDate = DateTimeOffset.Now;
            message.From = await _storage.GetContactAsync(message.FromId);

            var conversation = await _storage.FindConversationAsync(message.ConversationId, doctorId, patientId);
            if (conversation == null)
            {
                conversation = new Conversation()
                {
                    DoctorId = doctorId,
                    Doctor = await _storage.GetContactAsync(doctorId),
                    PatientId = patientId,
                    Patient = await _storage.GetContactAsync(patientId),
                };
            }
            CheckAccessToConversation(message, conversation);

            message.NewIdIfEmpty();
            message.ConversationId = conversation.Id;
            conversation.LastMessage = message;

            await _storage.AddMessageAsync(message);
            await _storage.UpsertConversationAsync(conversation);

            _notificationService.Post(ClientNotification.NewMessage(message));

            return message;
        }

        private void CheckAccessToConversation(Message message, Conversation conversation)
        {
            //todo: check message.FromId is equal to SecurityContext.User.Id

            if((message.FromId != conversation.PatientId && message.FromId != conversation.DoctorId) ||
               (message.ToId != conversation.PatientId && message.ToId != conversation.DoctorId))
            {
                throw new Helios.Common.ValidationException(
                    MessageServiceTextBuilder.CannotSendMessageToConversation(conversation.Id));
            }
        }
    }
}
