﻿using System;
using System.Threading.Tasks;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Models.Messages;
using JetBrains.Annotations;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class DocumentDbMessageRepository : IMessageRepository
    {
        private string CollectionName => CollectionNames.Messages;

        private readonly IDocumentDatabase _db;

        public DocumentDbMessageRepository([NotNull] IDocumentDatabase documentDatabase)
        {
            if (documentDatabase == null) throw new ArgumentNullException(nameof(documentDatabase));
            _db = documentDatabase;
        }

        public async Task<Message> GetAsync(Guid key)
        {
            return await _db.GetByIdAsync<Message>(CollectionName, key);
        }

        public  async Task<Message> AddAsync([NotNull] Message entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            await _db.AddAsync(CollectionName, entity);
            return entity;
        }

        public async Task<Message> UpdateAsync([NotNull] Message entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            return await _db.UpdateAsync(CollectionName, entity, entity.Id);
        }

        public async Task<Message> DeleteAsync(Guid key)
        {
            return await _db.DeleteByIdAsync<Message>(CollectionName, key);
        }
    }
}
