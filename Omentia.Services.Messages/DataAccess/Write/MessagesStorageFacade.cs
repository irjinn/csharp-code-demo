﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Http.Client;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class MessageStorageFacade : IMessageStorageFacade
    {
        private readonly IDocumentDatabase _db;
        private readonly IMessagingCacheFacade _cache;
        private readonly IContactsClient _contactsClient;

        public MessageStorageFacade(IDocumentDatabase db, IMessagingCacheFacade cache, IContactsClient contactsClient)
        {
            _db = db;
            _cache = cache;
            _contactsClient = contactsClient;
        }

        public async Task<Message> AddMessageAsync(Message message)
        {
            var result = await _db.AddAsync(CollectionNames.Messages, message);
            return _cache.Upsert(result);
        } 
        
        public async Task<Message> SetMessageIsReadAsync(Guid messageId)
        {
            var msg = _cache.MessageById(messageId);
            if (msg != null && msg.IsRead)
            {
                return msg;
            }

            var message = await _db.GetByIdAsync<Message>(CollectionNames.Messages, messageId);
            message.IsRead = true;

            //next: find workaround for race condition - no transaction in documentDb
            //currently it is not big deal
            var conversation = await _db.Async(db => db.Query<Conversation>(CollectionNames.Conversations)
                .Where(x => x.LastMessage != null && x.LastMessage.Id == messageId)
                .ToList()
                .FirstOrDefault());

            if (conversation != null)
            {
                conversation.LastMessage.IsRead = true;
                await _db.UpdateAsync(CollectionNames.Conversations, conversation, conversation.Id);
            }

            var result = await _db.UpdateAsync(CollectionNames.Messages, message, messageId);
            return _cache.Upsert(result);
        }

        public async Task<Conversation> FindConversationAsync(Guid? conversationId, Guid doctorId, Guid patientId)
        {
            Conversation result = null;
            if (conversationId.HasValue)
            {
                result = await _db.FindByIdAsync<Conversation>(CollectionNames.Conversations, conversationId.Value);
            }
            if (result == null)
            {
                await _db.EnsureCollectionExistsAsync(CollectionNames.Conversations);
                result = await _db.Async(db => db.Query<Conversation>(CollectionNames.Conversations)
                    .Where(x => x.DoctorId == doctorId && x.PatientId == patientId)
                    .ToList()
                    .FirstOrDefault());
            }
            return result;
        }

        public Task<Conversation> UpsertConversationAsync(Conversation conversation)
            => _db.UpsertAsync(CollectionNames.Conversations, conversation);
        
        public async Task<Contact> GetContactAsync(Guid id)
        {
            var contact = _cache.ContactById(id);
            if (contact == null)
            {
                contact = await _contactsClient.WithIdAsync(id);
                contact.EnsureNotNull(id);
                _cache.Upsert(contact);
            }
            return contact;
        }
    }
}