﻿using System;
using Helios.Common.Infrastructure.Cache;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess.Cache
{
    public class MessagingCacheFacade : IMessagingCacheFacade
    {
        public IObjectCache<Message> Messages { get; set; }
        public IObjectCache<Contact> ContactsById { get; set; }
        public IObjectCache<Contact> ContactsByName { get; set; }

        public Message MessageById(Guid id) => Messages?.Get(id);
        public Contact ContactById(Guid id) => ContactsById?.Get(id);
        public Contact ContactByName(string name) => ContactsByName?.Get(name);

        public Contact Upsert(Contact contact)
        {
            if (contact == null) return null;

            ContactsById?.Upsert(contact.Id, contact);

            if (!contact.Name.IsNullOrEmpty())
            {
                ContactsByName?.Upsert(contact.Id, contact);
            }
            return contact;
        }

        public Message Upsert(Message message)
        {
            if(message == null) return null;
            
            return Messages?.Upsert(message.Id, message);
        }

        public void Reset()
        {
            ContactsById?.Reset();
            ContactsByName?.Reset();
        }
    }
}