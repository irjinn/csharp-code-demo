﻿namespace Omentia.Services.Messages.DataAccess
{
    public static class CollectionNames
    {
        public static string Messages => "messages";
        public static string Contacts => "contacts";
        public static string Conversations => "conversations";
    }
}