﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Extensions;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class InMemoryConversationSource : IConversationSource
    {
        private readonly InMemoryStorage _storage;
        public InMemoryConversationSource(InMemoryStorage storage)
        {
            _storage = storage;
        }

        public Task<Conversation[]> Filter(ConversationFilter filter)
        {
            var conversations = _storage.Conversations.Values.AsEnumerable();
            if (filter.DoctorId.HasValue)
            {
                conversations = conversations.Where(x => x.DoctorId == filter.DoctorId.Value);
            }
            if (filter.PatientId.HasValue)
            {
                conversations = conversations.Where(x => x.PatientId == filter.PatientId.Value);
            }
            var userId = filter.DoctorId ?? filter.PatientId;
            var res = conversations
                .Select(conv => new
                {
                    Conversation = conv,
                    IsAnswered = conv.LastMessage?.FromId == userId,
                    IsRead = (conv.LastMessage?.IsRead ?? true) || conv.LastMessage?.FromId == userId
                }).ToList();

            if (filter.IsRead.HasValue)
            {
                res = res.Where(x => x.IsRead == filter.IsRead.Value).ToList();
            }
            if (filter.IsAnswered.HasValue)
            {
                res = res.Where(x => x.IsAnswered == filter.IsAnswered.Value).ToList();
            }
            return Task.FromResult(res.Select(x => x.Conversation).ToArray());
        }

        public Task<int> TotalUnread(Guid patientId)
        {
            var count = _storage.Conversations.Values
                .Where(x => x.PatientId == patientId)
                .Where(x => x.LastMessage?.ToId == patientId)
                .Count(x => x.LastMessage?.IsRead == false);

            return Task.FromResult(count);
        }

        public Task<Conversation> Details(Guid conversationId)
        {
            var conversation = _storage.Conversations.SafeGet(conversationId);
            conversation.CheckNotNull(conversationId);
            
            return GetDetailsAsync(conversation);
        }
        public async Task<Conversation> Details(Guid doctorId, Guid patientId)
        {
            var conversation = await FindOne(doctorId, patientId);
            if (conversation == null)
            {
                return null;
            }
            
            return await GetDetailsAsync(conversation);
        }

        private Task<Conversation> GetDetailsAsync(Conversation conversation)
        {
            conversation.CheckNotNull(conversation.Id);
            conversation.Messages = _storage.Messages.Values
                .Where(x => x.ConversationId == conversation.Id)
                .ToList();

            return Task.FromResult(conversation);
        }

        public Task<Conversation> FindOne(Guid doctorId, Guid patientId)
        {
            var conversation = _storage.Conversations.Values
                .FirstOrDefault(x => x.DoctorId == doctorId && x.PatientId == patientId);

            return Task.FromResult(conversation);
        }
    }
}