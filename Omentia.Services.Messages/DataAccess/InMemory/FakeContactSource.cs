﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Models.Contacts;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class FakeContactSource : IContactSource
    {
        private readonly ResourceContentReader _fileReader =
            new ResourceContentReader(typeof(FakeContactSource).Assembly, "Omentia.Services.Messages.Resources");

        public Task<Contact> FindByIdAsync(Guid id)
        {
            var contact = Doctors().FirstOrDefault(x => x.Id == id) ??
                          Patients().FirstOrDefault(x => x.Id == id);

            return Task.FromResult(contact);
        }

        public Task<Contact> FindByNameAsync(string name) => Task.FromResult(FindByName(name));
        public Contact FindByName(string name)
            => Doctors().Concat(Patients())
                .FirstOrDefault(x => x.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));

        public Task<Contact[]> DoctorsAsync() => Task.FromResult(Doctors());
        public Task<Contact[]> PatientsAsync() => Task.FromResult(Patients());
        public Task<Contact> GetDoctorAsync(Guid contactId) => Task.FromResult(GetDoctor(contactId));
        public Task<Contact> GetPatientAsync(Guid contactId) => Task.FromResult(GetPatient(contactId));

        private Contact[] Doctors()
        {
            var content = _fileReader.ReadAllText("contact-list.json");
            var items = content.FromJson<Contact[]>();
            return items;
        }

        private Contact[] Patients()
        {
            var content = _fileReader.ReadAllText("patients.json");
            var items = content.FromJson<Contact[]>();
            return items;
        }

        public Contact GetDoctor(Guid contactId)
        {
            return Doctors().FirstOrDefault(x => x.Id == contactId);
        }
        public Contact GetPatient(Guid contactId)
        {
            return Patients().FirstOrDefault(x => x.Id == contactId);
        }
    }
}