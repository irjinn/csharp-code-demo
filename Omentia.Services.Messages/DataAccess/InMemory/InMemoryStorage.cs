﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.DataAccess
{
    public class InMemoryStorage
    {
        public IDictionary<Guid, Conversation> Conversations { get; }
            = new ConcurrentDictionary<Guid, Conversation>();

        public IDictionary<Guid, Message> Messages { get; }
            = new ConcurrentDictionary<Guid, Message>();

        public IDictionary<Guid, Contact> Doctors { get; }
            = new FakeContactSource().DoctorsAsync().Result.ToDictionary(x => x.Id);

        public IDictionary<Guid, Contact> Patients { get; }
            = new FakeContactSource().PatientsAsync().Result.ToDictionary(x => x.Id);
    }
}