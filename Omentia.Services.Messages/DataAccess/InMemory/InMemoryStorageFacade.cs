﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Models;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class InMemoryStorageFacade : IMessageStorageFacade
    {
        private readonly InMemoryStorage _storage;
        public InMemoryStorageFacade(InMemoryStorage storage)
        {
            _storage = storage;
        }

        #region IMessagingStorageFacade
        
        public Task<Message> AddMessageAsync(Message message) => Task.FromResult(AddMessage(message));
        public Task<Message> SetMessageIsReadAsync(Guid messageId) => Task.FromResult(SetMessageIsRead(messageId));
        public Task<Conversation> FindConversationAsync(Guid? conversationId, Guid doctorId, Guid patientId) 
            => Task.FromResult(GetConversation(conversationId, doctorId, patientId));
        public Task<Conversation> UpsertConversationAsync(Conversation conversation)
            => Task.FromResult(UpsertConversation(conversation));


        public Task<Contact> GetPatientAsync(Guid patientId) => Task.FromResult(GetPatient(patientId));
        public Task<Contact> UpsertContactAsync(Contact contact)
        {
            var dictionary = contact.Type == ContactType.Patient
                ? _storage.Patients
                : _storage.Doctors;

            dictionary[contact.Id] = contact;

            return Task.FromResult(contact);
        }

        public Task<Contact> GetDoctorAsync(Guid doctorId) => Task.FromResult(GetDoctor(doctorId));
        #endregion

        public Message AddMessage(Message message)
        {
            message.NewIdIfEmpty();
            _storage.Messages[message.Id] = message;
            return message;
        }

        public Message SetMessageIsRead(Guid messageId)
        {
            var message = _storage.Messages.SafeGet(messageId);
            message.IsRead = true;
            var conversation = _storage.Conversations.Values
                .FirstOrDefault(x => x.LastMessage?.Id == messageId);

            if (conversation != null)
            {
                conversation.LastMessage.IsRead = true;
            }
            return message;
        }

        public Conversation GetConversation(Guid? conversationId, Guid doctorId, Guid patientId)
        {
            var conversation = _storage.Conversations.SafeGet(conversationId);
            if (conversation != null)
            {
                return conversation;
            }
            return _storage.Conversations.Values
                .FirstOrDefault(x => x.DoctorId == doctorId && x.PatientId == patientId);
        }

        public Conversation UpsertConversation(Conversation conversation)
        {
            conversation.NewIdIfEmpty();
            _storage.Conversations[conversation.Id] = conversation;
            return conversation;
        }

        public Task<Contact> GetContactAsync(Guid contactId) => Task.FromResult(GetContact(contactId));

        public Contact GetContact(Guid contactId)
            => GetDoctor(contactId) ?? GetPatient(contactId);  

        public Contact GetDoctor(Guid contactId) => _storage.Doctors.SafeGet(contactId);
        public Contact GetPatient(Guid contactId) => _storage.Patients.SafeGet(contactId)
            ?? _storage.Patients.Values.First().SetId(contactId);
    }
}