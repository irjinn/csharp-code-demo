﻿using System;
using System.Linq;
using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.DataAccess
{
    public static class QueryableExtensions
    {
        public static IQueryable<Conversation> Filter(this IQueryable<Conversation> queryable, ConversationFilter filter)
        {
            if (filter.PatientId.HasValue)
            {
                queryable = queryable.Where(x => x.PatientId == filter.PatientId.Value);
            }
            if (filter.DoctorId.HasValue)
            {
                queryable = queryable.Where(x => x.DoctorId == filter.DoctorId.Value);
            }
            //isRead only makes sense for user who is making query
            var userId = filter.DoctorId ?? filter.PatientId;
            if (filter.IsRead.HasValue && userId.HasValue)
            {
                queryable = queryable.FilterReadStatus(userId.Value, filter.IsRead.Value);
            }
            //isAnswered only makes sense for user who is making query
            if (filter.IsAnswered.HasValue && userId.HasValue)
            {
                queryable = queryable.FilterIsAnswered(userId.Value, filter.IsAnswered.Value);
            }
            return queryable;
        }

        public static IQueryable<Conversation> FilterReadStatus(
            this IQueryable<Conversation> queryable, 
            Guid userId, 
            bool isRead)
        {
            queryable = queryable
                .Where(x => x.LastMessage != null);

            return queryable.Where(x =>
                (x.LastMessage.FromId != userId && x.LastMessage.IsRead == isRead) ||
                (x.LastMessage.FromId == userId && isRead)); 
        }

        public static IQueryable<Conversation> FilterIsAnswered(
            this IQueryable<Conversation> queryable,
            Guid userId,
            bool isAnswered)
        {
            queryable = queryable
                .Where(x => x.LastMessage != null);

            if (isAnswered)
            {
                queryable = queryable
                    .Where(x => x.LastMessage.FromId == userId);
            }
            else
            {
                queryable = queryable
                    .Where(x => x.LastMessage.FromId != userId);
            }
            return queryable;
        }
    }
}