﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.DataAccess
{
    public class ConversationSource : IConversationSource
    {
        private readonly IDocumentDatabase _db;

        public ConversationSource(IDocumentDatabase db)
        {
            _db = db;
        }
        
        public async Task<Conversation> Details(Guid conversationId)
        {
            var conversation = await _db.GetByIdAsync<Conversation>(CollectionNames.Conversations, conversationId);
            conversation.Messages = await GetMessagesAsync(conversation.Id);
            return conversation;
        }

        public async Task<Conversation> Details(Guid doctorId, Guid patientId)
        {
            var conversation = await FindOne(doctorId, patientId);
            if (conversation == null)
            {
                return null;
            }
            conversation.Messages = await GetMessagesAsync(conversation.Id);

            return conversation;
        }

        public Task<Conversation> FindOne(Guid doctorId, Guid patientId)
        {
            return _db.Async(db => db.Query<Conversation>(CollectionNames.Conversations)
                .Where(x => x.DoctorId == doctorId && x.PatientId == patientId)
                .ToList()
                .FirstOrDefault());
        }

        public Task<int> TotalUnread(Guid patientId)
        {
            //currently there is no support for aggregates in documentDb
            //https://feedback.azure.com/forums/263030-documentdb/suggestions/6333963-add-support-for-aggregate-functions-like-count-su
            return _db.Async(db => db.Query<Conversation>(CollectionNames.Conversations)
                .Where(x => x.PatientId == patientId)
                .Where(x => x.LastMessage != null && x.LastMessage.FromId != patientId && x.LastMessage.IsRead == false)
                .ToList()
                .Count);
        }

        public Task<Conversation[]> Filter(ConversationFilter filter)
        {
            return _db.Async(db => db.Query<Conversation>(CollectionNames.Conversations)
                .Filter(filter)
                .ToArray()
                .OrderByDescending(x => x.LastMessage?.CreatedDate)
                .ToArray());
        }
        
        private Task<List<Message>> GetMessagesAsync(Guid conversationId)
        {
            return _db.Async(db => db.Query<Message>(CollectionNames.Messages)
                .Where(x => x.ConversationId == conversationId)
                .ToList()
                .OrderByDescending(x => x.CreatedDate)
                .ToList());
        }
    }
}