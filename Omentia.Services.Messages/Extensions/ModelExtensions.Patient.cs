﻿using Helios.Common.Models.Contacts;

namespace Omentia.Services.Messages.Extensions
{
    public static partial class ModelExtenions //Contact
    {
        public static bool IsPatient(this Contact contact)
            => contact.Type == ContactType.Patient;

        public static bool IsNotPatient(this Contact contact)
            => contact.Type != ContactType.Patient;
    }
}