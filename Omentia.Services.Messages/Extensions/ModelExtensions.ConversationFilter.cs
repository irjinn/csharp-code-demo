﻿using Helios.Common.Models.Messages;

namespace Omentia.Services.Messages.Extensions
{
    public static partial class ModelExtenions // ConversationFilter
    {
        public static ConversationFilter SetIsRead(this ConversationFilter filter, bool? isRead)
        {
            filter.IsRead = isRead;
            return filter;
        }

        public static ConversationFilter SetIsAnswered(this ConversationFilter filter, bool? isAnswered)
        {
            filter.IsAnswered = isAnswered;
            return filter;
        }
    }
}