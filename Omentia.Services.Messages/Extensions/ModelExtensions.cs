﻿using Helios.Common;

namespace Omentia.Services.Messages.Extensions
{
    public static partial class ModelExtenions
    {
        public static TModel CheckNotNull<TModel>(this TModel model, object id = null, string message = null)
        {
            if (model == null)
            {
                throw message == null 
                    ? new EntityNotFoundException(typeof(TModel).Name, id?.ToString())
                    : new EntityNotFoundException(message);
            }
            return model;
        }
    }
}