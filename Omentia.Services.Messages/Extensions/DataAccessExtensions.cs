﻿using System;
using System.Threading.Tasks;
using Helios.Common.Models.Messages;
using Omentia.Services.Messages.Interfaces;

namespace Omentia.Services.Messages.Extensions
{
    public static class DataAccessExtensions
    {
        public static Task<Conversation[]> WithDoctor(this IConversationSource source, Guid doctorId, bool? read = null, bool? answered = null)
            => source.Filter(ConversationFilter.WithDoctor(doctorId).SetIsRead(read).SetIsAnswered(answered));

        public static Task<Conversation[]> WithPatient(this IConversationSource source, Guid patientId, bool? read = null)
            => source.Filter(ConversationFilter.WithPatient(patientId).SetIsRead(read));
    }
}