﻿using System;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;

namespace Omentia.Services.Messages.Extensions
{
    public static partial class ModelExtenions //Messages
    {
        public static Message SetFrom(this Message message, Contact from)
        {
            message.FromId = from.Id;
            message.From = from;
            return message;
        }
        public static Message SetFrom(this Message message, Guid fromId)
        {
            message.FromId = fromId;
            return message;
        }

        public static Message SetTo(this Message message, Contact to)
        {
            message.ToId = to.Id;
            return message;
        }
        public static Message SetTo(this Message message, Guid toId)
        {
            message.ToId = toId;
            return message;
        }

        public static Message SetBody(this Message message, string content)
        {
            message.Body = content;
            return message;
        }

        public static Message AppendBody(this Message message, string content)
        {
            message.Body = message.Body.IsNullOrEmpty()
                ? $"{content}"
                : $"{message.Body}{Environment.NewLine}{content}";

            return message;
        }


        public static Message CreateReply(this Message message)
            => new Message
            {
                FromId = message.ToId,
                From = message.To,
                ToId = message.FromId,
                To = message.From,
            };

        public static bool IsBetweenDoctorAndPatient(this Message msg)
        {
            return msg.From != null
                   && msg.To != null
                   && msg.From.IsPatient() != msg.To.IsPatient();
        }
    }
}