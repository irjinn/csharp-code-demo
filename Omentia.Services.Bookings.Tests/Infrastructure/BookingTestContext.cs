﻿using System;
using Autofac;
using Helios.Common.Infrastructure.Settings;
using Helios.Common.Notifications;
using Moq;
using Omentia.Common.TestUtils;
using Omentia.Services.Bookings.Api;
using Omentia.Services.Bookings.DataAccess.Sql;

namespace Omentia.Services.Bookings.Tests.Infrastructure
{
    public class BookingTestContext : MvcIntegrationTestFixture, IDisposable
    {
        public BookingTestContext() : base(ConfigureContainer)
        {
            BookingSettings.Initialize(ConfigFile.Instance);
            BookingsDb.Initialize();
        }

        public override void CleanUp()
        {
            //DbContext.Database.Delete();
        }

        private static void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterModule<BookingApiModule>();

            var notificationService = new Mock<INotificationService>().Object;
            builder.RegisterInstance(notificationService).As<INotificationService>();
        }
    }
}