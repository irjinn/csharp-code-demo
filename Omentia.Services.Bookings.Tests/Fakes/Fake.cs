﻿using System;
using FizzWare.NBuilder.Generators;
using Helios.Common;
using Helios.Common.Fakes;
using Helios.Common.Models.Bookings;
using Helios.Common.Models.Const;
using Helios.Common.Utils;

namespace Omentia.Services.Bookings.Tests
{
    public static class Fake
    {
        public static Guid CareUnitId { get; } = Guid.NewGuid();
        public static IFakesSource<Timeslot> Timeslots = new FakesSource<Timeslot>((index, model) =>
        {
            var startDate = DateTimeOffset.Now.AddDays(1).Date
                .AddHours(8).AddMinutes(index * BookingDefaults.TimeslotDurationMinutes);

            model.CareUnitId = CareUnitId;
            model.DoctorId = GuidId.New();
            model.SetStartTime(startDate, BookingDefaults.TimeslotDurationMinutes);
            //model.Outcome = Generate.EnumValue<DoctorActivity>().AsNullable().OrNull();
            model.ChangeDate = Generate.Date.InPast().AsNullable().OrNull();
        });

        public static IFakesSource<Booking> Bookings = new FakesSource<Booking>(model =>
        {
            model.PatientId = GuidId.New();
            model.PatientComment = Generate.Paragraph();
            model.Activity = Generate.EnumValue<DoctorActivity>();
            model.CreatedDate = Generate.Date.InPast();
            var expirationDate = model.CreatedDate.AddMinutes(BookingDefaults.BookingExpirationMinutes);
            model.BookingDate = Generate.Date.InRange(model.CreatedDate, expirationDate).AsNullable().OrNull();
            if (model.BookingDate == null)
            {
                model.ExpirationDate = expirationDate;
            }
            model.ChangeDate = Generate.Date.InPast(model.CreatedDate).AsNullable().OrNull();
        });

        public static IFakesSource<VideoSession> VideoSessions = new FakesSource<VideoSession>(model =>
        {
            model.DoctorId = GuidId.New();
            model.PatientId = GuidId.New().AsNullable().OrNull();
            model.PlannedEndDate = model.CreatedDate.AddMinutes(BookingDefaults.TimeslotDurationMinutes);
            model.ActualEndDate = Generate.Date.InRange(model.CreatedDate, model.PlannedEndDate);
            model.ServiceSessionId = GetRandom.String(GetRandom.Int(10, 20));
            model.ServiceAccessToken = GetRandom.String(GetRandom.Int(10, 25));
            model.ServiceApiKey = GetRandom.String(GetRandom.Int(5, 10));
            model.CreatedDate = Generate.Date.InPast();
            model.ChangeDate = model.ActualEndDate ?? Generate.Date.InPast(model.CreatedDate);
        });

        public static IFakesSource<TimeInterval> TimeIntervals = new FakesSource<TimeInterval>(model =>
        {
            model.To = Generate.DayTimeSpan();
            model.From = Generate.TimeSpan(model.To);
        });
        public static IFakesSource<WeekSchedule> WeekSchedules = new FakesSource<WeekSchedule>(model =>
        {
            model.DoctorId = Guid.NewGuid();
            model.CareUnitId = CareUnitId;
            model.Monday = new [] { TimeIntervals.Create()};
            model.Tuesday = new [] {TimeIntervals.Create()};
            model.Wednesday = new [] {TimeIntervals.Create()};
            model.Thursday = new [] {TimeIntervals.Create()};
            model.Friday = new [] {TimeIntervals.Create()};
            model.Saturday = new [] {TimeIntervals.Create()};
            model.Sunday = new [] {TimeIntervals.Create()};
        });
    }
}