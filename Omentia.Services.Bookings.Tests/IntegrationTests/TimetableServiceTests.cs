﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Tests.Infrastructure;
using Shouldly;
using Xunit;

namespace Omentia.Services.Bookings.Tests.IntegrationTests
{
    public class TimetableServiceTests : IClassFixture<BookingTestContext>
    {
        private BookingTestContext Context { get; }
        public IBookingSource BookingSource => Context.Resolve<IBookingSource>();
        public IBookingService BookingService => Context.Resolve<IBookingService>();
        public ITimetableService TimetableService => Context.Resolve<ITimetableService>();
        public ITimeslotSource TimeslotSource => Context.Resolve<ITimeslotSource>();
        private IRequestContext RequestContext => Context.Resolve<IRequestContext>();

        public TimetableServiceTests(BookingTestContext testContext)
        {
            Context = testContext;
        }

        [Fact]
        public void TimetableService_SetDoctorSchedule_GetScheduleReturnsSameSchedule()
        {
            //Arrange
            var expected = Fake.WeekSchedules.Create();

            //Act
            TimetableService.SetDoctorSchedule(expected);
            var actual = TimetableService.GetDoctorSchedule(expected.DoctorId);

            //Assert
            actual.ShouldNotBe(null);
            foreach (DayOfWeek day in Enum.GetValues(typeof(DayOfWeek)))
            {
                var actualTime = actual.GetWorkingTime(day);
                var expectedTime = expected.GetWorkingTime(day);
                actualTime.ShouldBe(expectedTime);
                actual.DoctorId.ShouldBe(expected.DoctorId);
            }
        }

        [Fact]
        public async Task TimetableService_SetDoctorSchedule_CreatesTimeslots()
        {
            //Arrange
            var date = DateTimeOffset.Now.AddDays(1).Date;
            var day = date.DayOfWeek;
            var doctorId = Guid.NewGuid();
            var schedule = new WeekSchedule().SetDoctorId(doctorId).SetWorkingTime(day, "8:55", "10:10");
            
            var expectedIntervals = new[]
            {
                new TimeInterval("9:00", "9:15"),
                new TimeInterval("9:15", "9:30"),
                new TimeInterval("9:30", "9:45"),
                new TimeInterval("9:45", "10:00"),
            };

            //Act
            TimetableService.SetDoctorSchedule(schedule);
            await RequestContext.CommitChangesAsync();

            var allTimeslots = TimeslotSource.Find(TimeslotFilter.WithDoctor(doctorId)).ToList();
            var timeslots = allTimeslots.Where(x => x.Date == date);
            var actualTimeIntervals = timeslots.Select(x => x.GetTimeInterval())
                .OrderBy(x => x.From)
                .ToArray();
            
            //Assert
            actualTimeIntervals.ShouldBe(expectedIntervals);
        }

        [Fact]
        public async Task TimetableService_UpdateDoctorSchedule_MarksUnusedTimeslotsAsUnavailable()
        {
            //Arrange
            var date = DateTimeOffset.Now.AddDays(1).Date;
            var day = date.DayOfWeek;
            var doctorId = Guid.NewGuid();
            var originalSchedule = new WeekSchedule().SetDoctorId(doctorId).SetWorkingTime(day, "8:55", "10:10");
            TimetableService.SetDoctorSchedule(originalSchedule);
            await RequestContext.CommitChangesAsync();

            var newSchedule = new WeekSchedule().SetDoctorId(doctorId).SetWorkingTime(day, "9:05", "10:10");
            var expectedIntervals = new[]
            {
                new TimeInterval("9:15", "9:30"),
                new TimeInterval("9:30", "9:45"),
                new TimeInterval("9:45", "10:00"),
            };

            //Act
            TimetableService.SetDoctorSchedule(newSchedule);
            await RequestContext.CommitChangesAsync();

            var allTimeslots = TimeslotSource.Find(TimeslotFilter.WithDoctor(doctorId));
            var timeslots = allTimeslots.Where(x => x.Date == date);
            var actualTimeIntervals = timeslots
                .Where(x => x.Availability != DoctorActivity.Unavailable)
                .Select(x => x.GetTimeInterval())
                .OrderBy(x => x.From)
                .ToArray();

            //Assert
            actualTimeIntervals.ShouldBe(expectedIntervals);
        }
    }
}