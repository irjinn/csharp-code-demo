﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Helios.Common.Fakes;
using Helios.Common.Models.Bookings;
using Omentia.Services.Bookings.Interfaces;
using Omentia.Services.Bookings.Tests.Infrastructure;
using Shouldly;
using Xunit;

namespace Omentia.Services.Bookings.Tests.IntegrationTests
{
    public class BookingServiceTests : IClassFixture<BookingTestContext>
    {
        private BookingTestContext Context { get; }
        public IBookingSource BookingSource => Context.Resolve<IBookingSource>();
        public ITimeslotSource TimeslotSource => Context.Resolve<ITimeslotSource>();
        public IBookingService Service => Context.Resolve<IBookingService>();
        public ITimetableService TimetableService => Context.Resolve<ITimetableService>();
        private IRequestContext RequestContext => Context.Resolve<IRequestContext>();

        public BookingServiceTests(BookingTestContext testContext)
        {
            Context = testContext;
        }

        private async Task<Timeslot> CreateTimeslotAsync(Action<Timeslot> customization = null)
        {
            var timeslot = Fake.Timeslots.Create();
            timeslot.Outcome = null;
            customization?.Invoke(timeslot);
            var result = TimetableService.CreateTimeslot(timeslot);
            await RequestContext.CommitChangesAsync();
            return result;
        }

        private IEnumerable<Booking> ActiveBookings(Guid patientId)
            => BookingSource.Find(
                BookingFilter
                    .WithPatient(patientId)
                    .IsActive(true));

        [Fact]
        public async Task BookingSource_FindAvailable_ReturnAvailableTimeslot()
        {
            //Arrange
            var doctorId = Guid.NewGuid();
            var expected = await CreateTimeslotAsync(x =>
            {
                x.DoctorId = doctorId;
                x.Availability = DoctorActivity.PhysicalVisitAndVideo;
            });
            await RequestContext.CommitChangesAsync();
            //Act
            var actual = TimeslotSource.Find(TimeslotFilter.WithDoctor(doctorId).AvailableForVideo()).FirstOrDefault();

            //Assert
            actual.ShouldNotBe(null);
        }   

        [Fact]
        public async Task BookingService_BookVideoCall_ReturnCorrectBooking()
        {
            //Arrange
            var patientId = Guid.NewGuid();
            var timeslot = await CreateTimeslotAsync();
            var expectedActivity = DoctorActivity.VideoCall;

            //Act
            var booking = Service.BookTimeslot(patientId, timeslot.Id, expectedActivity);
            
            //Assert
            booking.Activity.ShouldBe(expectedActivity);
            booking.PatientId.ShouldBe(patientId);
        }   
        
        [Fact]
        public async Task BookingService_BookVideoCall_ReturnedFromGetPatientBookings()
        {
            //Arrange
            var patientId = Guid.NewGuid();
            var timeslot = await CreateTimeslotAsync();
            var expectedActivity = DoctorActivity.VideoCall;

            //Act
            Service.BookTimeslot(patientId, timeslot.Id, expectedActivity);
            await RequestContext.CommitChangesAsync();

            var activeBookings = ActiveBookings(patientId).ToList();

            //Assert
            activeBookings.Count.ShouldBe(1);
            var activeBooking = activeBookings.Single();
            activeBooking.Activity.ShouldBe(expectedActivity);
        }

        [Fact]
        public async Task BookingService_CommentBooking_BookingIsUpdated()
        {
            //Arrange
            var patientId = Guid.NewGuid();
            var timeslot = await CreateTimeslotAsync();
            var booking = Service.BookTimeslot(patientId, timeslot.Id, DoctorActivity.VideoCall);
            var expectedComment = Generate.Paragraph();
            await RequestContext.CommitChangesAsync();
            
            //Act
            Service.SetBookingComment(patientId, booking.Id, expectedComment);
            await RequestContext.CommitChangesAsync();

            var activeBookings = ActiveBookings(patientId);
            var activeBooking = activeBookings.FirstOrDefault();

            //Assert
            activeBooking.ShouldNotBe(null);
            activeBooking.PatientComment.ShouldBe(expectedComment);
        }

        [Fact]
        public async Task BookingService_PayForBooking_PayingStatusIsUpgraded()
        {
            //Arrange
            var patientId = Guid.NewGuid();
            var timeslot = await CreateTimeslotAsync();
            var booking = Service.BookTimeslot(patientId, timeslot.Id, DoctorActivity.VideoCall);
            await RequestContext.CommitChangesAsync();

            //Act
            Service.PayForBooking(patientId, booking.Id, new BookingPaymentInputModel { Type = PaymentType.CreditCard });
            await RequestContext.CommitChangesAsync();

            var actualBookings = ActiveBookings(patientId);
            var actualBooking = actualBookings.FirstOrDefault();

            //Assert
            actualBooking.ShouldNotBe(null);
            actualBooking.Payment.ShouldNotBe(null);
            actualBooking.Payment.PaymentType.ShouldBe(PaymentType.CreditCard);
            actualBooking.Payment.PaymentStatus.ShouldBe(PaymentStatus.Complete);
        }

        [Fact]
        public async Task BookingService_DeleteBooking_UpdatesTimeslotActualBooking()
        {
            //Arrange
            var patientId = Guid.NewGuid();
            var timeslot = await CreateTimeslotAsync();
            var expectedActivity = DoctorActivity.VideoCall;
            var booking = Service.BookTimeslot(patientId, timeslot.Id, expectedActivity);
            await RequestContext.CommitChangesAsync();

            //Act
            Service.DeleteBooking(booking.Id);
            await RequestContext.CommitChangesAsync();

            var actualTimeslot = TimeslotSource.Find(TimeslotFilter.WithDoctor(timeslot.DoctorId))
                .FirstOrDefault(x => x.Id == timeslot.Id);

            //Assert
            actualTimeslot.ShouldNotBe(null);
            actualTimeslot.ActualBooking.ShouldBeNull();
        }
    }
}