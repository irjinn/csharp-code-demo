﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;

namespace Omentia.Services.Messages.Api
{
    public class AppContainer
    {
        /// <summary>
        /// Application DI container instance.
        /// </summary>
        public static IContainer Instance { get; private set; }

        /// <summary>
        /// Make registrations and build app container.
        /// </summary>
        public static void Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()).PropertiesAutowired();
            builder.RegisterModule(new MessagesApiModule());

            Instance = builder.Build();
        }
    }
}