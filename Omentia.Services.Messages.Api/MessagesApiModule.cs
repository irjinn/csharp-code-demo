﻿using System;
using System.Collections.Generic;
using Autofac;
using Helios.Common.DataAccess.DocumentDb;
using Helios.Common.Infrastructure;
using Helios.Common.Infrastructure.Cache;
using Helios.Common.Infrastructure.Settings;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Omentia.Services.Messages.DataAccess;
using Omentia.Services.Messages.DataAccess.Cache;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Services;

namespace Omentia.Services.Messages.Api
{
    public class MessagesApiModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterModule<DocumentDbModule>(); // for IDocumentStorage
            builder.RegisterModule<NotificationsModule>();
            builder.RegisterModule<HttpClientModule>(); // for IContactsClient

            builder
                .RegisterType<MessageService>()
                .As<IMessageService>()
                .SingleInstance();

            builder
                .Register(c => new MessagingCacheFacade
                {
                    Messages = new ObjectCache<Message>(CollectionNames.Messages),
                    ContactsById = new ObjectCache<Contact>(CollectionNames.Contacts),
                    ContactsByName = new ObjectCache<Contact>(CollectionNames.Contacts),
                })
                .As<IMessagingCacheFacade>()
                .SingleInstance();
            
            builder
                .RegisterType<MessageValidator>()
                .As<IMessageValidator>();
            
            RegisterDocumentDbStorage(builder);
            RegisterAutoReply(builder, ConfigFile.Instance);
        }

        private void RegisterDocumentDbStorage(ContainerBuilder builder)
        {
            builder
                .RegisterType<ConversationSource>()
                .As<IConversationSource>();

            builder
                .RegisterType<MessageStorageFacade>()
                .As<IMessageStorageFacade>();

            builder
                .Register(c => new DocumentDatabaseConfiguration { DatabaseName = "messages" })
                .As<IDocumentDatabaseConfiguration>();

            builder
                .RegisterType<LazyInitializedDocumentDatabase>()
                .As<IDocumentDatabase>();
        }

        private void RegisterAutoReply(ContainerBuilder builder, ISettingsSource configuration)
        {
            var userId = configuration.GetValue<Guid>(SettingsKeys.Messages.AutoReply.PatientId,
                new Guid("39937006-b964-47ed-8989-62707ede1f63"));

            var doctorId = configuration.GetValue<Guid>(SettingsKeys.Messages.AutoReply.DoctorId,
                new Guid("d33fe82e-3217-45a3-8732-8db2ad3c931d"));

            var parameters = new AutoReplyConfiguration()
            {
                Listeners = new List<Guid> { userId, doctorId },
                MessageReadDelay = 1,
                MessageReplyDelay = 5,
            };
            builder.RegisterInstance(parameters);
            builder.RegisterType<AutoReplyService>().As<IAutoReplyService>();
            builder.RegisterType<AutoReplyBot>().AsSelf();
        }
        private void RegisterInMemoryStorage(ContainerBuilder builder)
        {
            builder
                .RegisterType<InMemoryStorage>()
                .AsSelf()
                .SingleInstance();

            builder
                .RegisterType<InMemoryConversationSource>()
                .As<IConversationSource>()
                .SingleInstance();

            builder
                .RegisterType<InMemoryStorageFacade>()
                .As<IMessageStorageFacade>();
        }
    }
}