﻿using System;
using System.Web;
using System.Web.Http;
using Autofac.Integration.WebApi;
using Helios.Common.Infrastructure;
using Omentia.Common.Web;
using Omentia.Common.Web.Swagger;

namespace Omentia.Services.Messages.Api
{
    /// <summary>
    /// Messaging web api application.
    /// </summary>
    public class WebApiApplication : HttpApplication
    {
        /// <summary>
        /// Entry point for application.
        /// </summary>
        protected void Application_Start()
        {
            LogConfiguration.Initialize();
            Log.Debug("application starting");
            this.Error += OnError;

            AppContainer.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(AppContainer.Instance);

            GlobalConfiguration.Configuration.MessageHandlers.Add(new RequestLogHandler());
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configure(
                c => SwaggerConfig.RegisterDefault(c, "Messages API", "Omentia.Services.Messages.Api.XML"));

            Log.Info("application started");
        }

        /// <summary>
        /// Log unhandled exceptions 
        /// </summary>
        private void OnError(object sender, EventArgs args)
        {
            var prop = sender?.GetType().GetProperty("Context"); //weird hack for asp.web api
            var context = prop?.GetValue(sender) as HttpContext;

            Log.Error(message: "unhandled exception occured", data: args, exception: context?.Error);
        }
    }
}