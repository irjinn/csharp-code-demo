﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Http.Client;
using Helios.Common.Models.Contacts;
using Omentia.Common.Web;
using Omentia.Common.Web.Swagger;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Messages.Api.Controllers
{
    [SwaggerHide]
    [RoutePrefix("contacts")] //for administrative tasks, do not use from client apps
    public class ContactController : ControllerBase
    {
        public IContactsClient ContactsClient { get; set; }

        [HttpGet]  
        [Route("patients/{id:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<Contact>))]
        public async Task<HttpResponseMessage> GetPatient(Guid id)
        {
            var contact = await ContactsClient.WithIdAsync(id);
            return Success(contact);
        }

        [HttpGet]  
        [Route("patients")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<Contact[]>))]
        public async Task<HttpResponseMessage> GetPatients(Guid? careUnitId = null)
        {
            // todo: force doctor client to pass careUnitId
            var id = careUnitId ?? new Guid("3ccd46eb-4cc8-4085-9934-f3645b43c783");
            var contacts = await ContactsClient.PatientsAsync(id);
            return Success(contacts);
        }
        
        [HttpGet]
        [Route("doctors/{id:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<Contact>))]
        public async Task<HttpResponseMessage> GetDoctor(Guid id)
        {
            var contact = await ContactsClient.WithIdAsync(id);
            return Success(contact);
        }
    }
}