﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Models;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Services.Messages.Extensions;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Services;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Messages.Api.Controllers
{
    [RoutePrefix("doctors")]
    public class DoctorController : ControllerBase
    {
        public IConversationSource ConversationsSource { get; set; }
        public IMessageService MessageService { get; set; }
        public AutoReplyBot AutoReplyBot { get; set; }

        [HttpGet]
        [Route("{doctorId:guid}/conversations")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<ConversationViewModel[]>))]
        public async Task<HttpResponseMessage> Conversations(Guid doctorId, bool? read = null, bool? answered = null)
        {
            var conversations = await ConversationsSource.WithDoctor(doctorId, read, answered);
            var viewModels = conversations.MapToArray(Map.Conversations.ToViewModel);
            Array.ForEach(viewModels, x => x.Messages = null); //temp
            return Success(viewModels);
        }

        [HttpGet]
        [Route("{doctorId:guid}/conversations/{conversationId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<ConversationViewModel>))]
        public async Task<HttpResponseMessage> Conversation(Guid doctorId, Guid conversationId)
        {
            var conversation = await ConversationsSource.Details(conversationId);
            var viewModel = Map.Conversations.ToViewModel(conversation);
            return Success(viewModel);
        }

        [HttpGet]
        [Route("{doctorId:guid}/conversations/patients/{patientId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<ConversationViewModel>))]
        public async Task<HttpResponseMessage> ConversationWithPatient(Guid doctorId, Guid patientId)
        {
            var conversation = await ConversationsSource.Details(doctorId, patientId);
            var viewModel = Map.Conversations.ToViewModel(conversation);
            return Success(viewModel);
        }

        [HttpPost]
        [Route("{doctorId:guid}/messages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<MessageViewModel>))]
        public async Task<HttpResponseMessage> SendMessage(Guid doctorId, MessageInputModel message)
        {
            var msg = InputModel.ToModel(message, doctorId);
            msg = await MessageService.SendDoctorMessageAsync(msg);
            AutoReplyBot?.Async(x => x.HandleMessage(msg)).FireAndForget();
            var viewModel = Map.Messages.ToViewModel(msg);
            return Success(viewModel);
        }

        [HttpPost]
        [Route("{doctorId:guid}/messages/{messageId:guid}/read")]
        [SwaggerResponse(HttpStatusCode.OK)]
        public async Task MarkAsRead(Guid doctorId, Guid messageId)
        {
            await MessageService.SetMessageIsRead(messageId);
        }
    }
}