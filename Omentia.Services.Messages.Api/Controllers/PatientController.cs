﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Helios.Common.Http;
using Helios.Common.Http.Client;
using Helios.Common.Models;
using Helios.Common.Models.Contacts;
using Helios.Common.Models.Messages;
using Helios.Common.Utils;
using Omentia.Common.Web;
using Omentia.Common.Web.Extensions;
using Omentia.Services.Messages.Extensions;
using Omentia.Services.Messages.Interfaces;
using Omentia.Services.Messages.Services;
using Swashbuckle.Swagger.Annotations;

namespace Omentia.Services.Messages.Api.Controllers
{
    [RoutePrefix("patients")]
    public class PatientController : ControllerBase
    {
        public IConversationSource ConversationsSource { get; set; }
        public IContactsClient ContactsClient { get; set; }
        public IMessageService MessageService { get; set; }
        public AutoReplyBot AutoReplyBot { get; set; }

        [HttpGet]
        [Route("{userId:guid}/contacts")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<Contact[]>))]
        public async Task<HttpResponseMessage> Contacts(Guid userId, string type = null)
        {
            var contacts = await ContactsClient.AvailableAsync(Request.AuthToken());
            var filteredContacts = type == null 
                ? contacts 
                : contacts.Where(x => 
                    string.Equals(x.Type.ToString(), type, StringComparison.InvariantCultureIgnoreCase));

            var viewModels = filteredContacts;
            return Success(viewModels);
        }

        [HttpGet]
        [Route("{userId:guid}/conversations")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<ConversationViewModel[]>))]
        public async Task<HttpResponseMessage> Conversations(Guid userId)
        {
            var conversations = await ConversationsSource.WithPatient(userId);
            var viewModels = conversations.MapToArray(
                x => Map.Conversations.ToViewModel(x, mapMessages: false));

            return Success(viewModels);
        }
        
        [HttpGet]
        [Route("{userId:guid}/conversations/unread/count")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<int>))]
        public async Task<HttpResponseMessage> TotalUnread(Guid userId)
        {
            var count = await ConversationsSource.TotalUnread(userId);
            return Success(count);
        }

        [HttpGet]
        [Route("{userId:guid}/conversations/{conversationId:guid}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<ConversationViewModel>))]
        public async Task<HttpResponseMessage> Conversation(Guid userId, Guid conversationId)
        {
            var conversation = await ConversationsSource.Details(conversationId);
            var viewModel = Map.Conversations.ToViewModel(conversation);
            return Success(viewModel);
        }

        [HttpPost]
        [Route("{userId:guid}/messages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(AjaxResponse<MessageViewModel>))]
        public async Task<HttpResponseMessage> SendMessage(Guid userId, [FromBody] MessageInputModel message)
        {
            var msg = InputModel.ToModel(message, userId);
            msg = await MessageService.SendPatientMessageAsync(msg);
            AutoReplyBot?.Async(x => x.HandleMessage(msg)).FireAndForget();
            var viewModel = Map.Messages.ToViewModel(msg);
            return Success(viewModel);
        }

        [HttpPost]
        [Route("{userId:guid}/messages/{messageId:guid}/read")]
        [SwaggerResponse(HttpStatusCode.OK)]
        public async Task MarkAsRead(Guid userId, Guid messageId)
        {
            await MessageService.SetMessageIsRead(messageId);
        }
    }
}