﻿using System.IdentityModel.Tokens.Jwt;
using Helios.Common.Infrastructure.Settings;
using Helios.Common.Utils;
using IdentityServer3.AccessTokenValidation;
using Microsoft.Owin;
using Omentia.Services.Messages.Api;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Omentia.Services.Messages.Api
{
    public class Startup
    {
        internal static string StsEndpoint = ConfigFile.Instance.GetValue<string>(SettingsKeys.Endpoints.Sts);
        internal static string Authority => UrlUtils.Concat(StsEndpoint, "/identity");

        public void Configuration(IAppBuilder app)
        {
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var authenticationOptions = new IdentityServerBearerTokenAuthenticationOptions
            {
                Authority = Authority,
                ValidationMode = ValidationMode.ValidationEndpoint,
                RequiredScopes = new[] { "tenants" }
            };
            app.UseIdentityServerBearerTokenAuthentication(authenticationOptions);
        }
    }
}